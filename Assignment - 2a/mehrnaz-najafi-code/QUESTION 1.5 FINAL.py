
# coding: utf-8

# In[1]:

def getLabelPos (word, j):
    c = word[j]
    
    labels = "etainoshrd"
    for index in range(len(labels)):
        if(c == labels[index]):
            return index
    return -1

def computeNodePotentialByWord(i, word):
    param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)

    feature     = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)

    ##step_ijf = 0
    positions = []
    step_ijf = 0
    for j in range(len(word)):
        position_label = []
        cf  = 0
        value = 0
        for c in range(10):
            x_ijf = step_ijf
            for f in range(0, 321):
                # Wcf * xijf
                temp = float(parameters[cf]) * float(observations[x_ijf])
                #QUESTION: Am I right?
                #if(getLabelByPos(word, j) != c):
                #    temp = 0
                if(getLabelPos(word,j) != c):
                    temp = 0
                value = value + temp
                cf = cf + 1
                x_ijf = x_ijf + 1
        position_label.append(value)

        step_ijf = step_ijf + 321
        positions.append(position_label)
        
    #print (positions)
    return (positions)

##computeNodePotentialByWord(1, "sala")


# In[2]:


def computeTransitionPotentialByWord(i, word):
    param = open('/Users/anahita/Desktop/transition-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)
    
    transition_position = []
    for j in range(len(word) - 1): 
        w = float(parameters[getLabelPos(word, j) * 10 + getLabelPos(word, j + 1)])
        transition_position.append(w)
        
    return transition_position


# In[3]:

def computeEnergyForLabelSeq(i, word):
    #we want to compute the value for the first three test words
    file = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
        
    #first summation term
    node_positions = computeNodePotentialByWord(i, word)
    term1 = node_positions[0][0]
    #compute \sum \limits_{j = 1}^{L_{i}}
    for j in range(1, len(word)):
        term1 = term1 + node_positions[j][0]
    #print("term1")
    #print(term1)
    
    
    #second summation term
    transition_positions = computeTransitionPotentialByWord(i, word)
    term2 = float(transition_positions[0])
    #compute \sum \limits_{j = 1}^{L_{i} - 1}
    for j in range(1, len(word) - 1): 
        term2 = term2 + float(transition_positions[j])
    #print ("term2")
    #print (term2)
    
    return (term1 + term2)


# In[4]:

import math
import itertools
#we want to compute \sum \limits_{Y'} exp (-E(x, Y'))
def computePartitionFunction(i):
    y           = open('/Users/anahita/Desktop/test_words.txt')
    word = ''
    for l in range(i):
        word = y.readline()
    # \n; so, len - 1 (we want to know the length of the true label)
    #word = word[0:len(word)-1]
    l = len(word) - 1
    
    result = 0

    partition = float(0)
    labels = []
    labels.append("e")
    labels.append("t")
    labels.append("a")
    labels.append("i")
    labels.append("n") 
    labels.append("o")
    labels.append("s")
    labels.append("h")
    labels.append("r")
    labels.append("d")
    y_all = itertools.product(labels, repeat = l)
    
    #log_partition = float(0)
    for y in y_all:
        label = ''.join(y)
        #print(label)
        temp = computeEnergyForLabelSeq(i, label)
        partition = partition + math.exp(temp)  
    #log_partition = math.log(partition)
    
    return partition


# In[5]:

def computeJointLabeling(i, word, second_part):
    #first part
    first_part = math.exp(computeEnergyForLabelSeq(i, word))
    
    return (first_part / second_part)

#print(computeJointLabeling(1, "that"))


# In[ ]:

import math
#computes marginals for the ith test word
def computeMarginals(i):
    y           = open('/Users/anahita/Desktop/test_words.txt')
    word = ''
    for l in range(i):
        word = y.readline()
    # \n; so, len - 1 (we want to know the length of the true label)
    #word = word[0:len(word)-1]
    l = len(word) - 1
    
    #second part (partition function)
    second_part = computePartitionFunction(i)
    #print(math.log(second_part))
    
    labels = []
    labels.append("e")
    labels.append("t")
    labels.append("a")
    labels.append("i")
    labels.append("n") 
    labels.append("o")
    labels.append("s")
    labels.append("h")
    labels.append("r")
    labels.append("d")
    y_all = itertools.product(labels, repeat = l)
    
    joint_labelings = []
    joint_label_prob = []
    for y in y_all:
        label = ''.join(y)
        #print(label)
        joint_prob = computeJointLabeling(i, label, second_part)
        joint_labelings.append(label)
        joint_label_prob.append(joint_prob)
    #log_partition = math.log(partition)
    
    position_marginals = []
    for j in range(4):
        label_position_marginal = []
        for c in range(10):
            summation = 0
            for count in range(len(joint_labelings)):
                word = joint_labelings[count]
                if(getLabelPos(word,j) == c):
                    summation = summation + joint_label_prob[count]
            label_position_marginal.append(summation)
        position_marginals.append(label_position_marginal)

    return position_marginals

print(computeMarginals(1))


# In[ ]:



