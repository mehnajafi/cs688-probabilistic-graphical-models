
# coding: utf-8

# In[16]:

#Question 2.1
def getLabelPos (word, j):
    c = word[j]
    
    labels = "etainoshrd"
    for index in range(len(labels)):
        if(c == labels[index]):
            return index
    return -1

#returns a vector with len(word) columns, each represents potential value for each position
def computeNodePotentialByWord(j, i):
    param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)

    feature     = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)

    ##step_ijf = 0
    #positions = []
    step_ijf = 0
    for index in range(j):
        step_ijf = step_ijf + 321
    ##for j in range(len(word)):
    position_label = []
    for yj in range(10):
        cf  = 0
        value = 0
        for c in range(10):
            x_ijf = step_ijf
            for f in range(0, 321):
                # Wcf * xijf
                temp = float(parameters[cf]) * float(observations[x_ijf])
                #QUESTION: Am I right?
                #if(getLabelByPos(word, j) != c):
                #    temp = 0
                if(yj != c):
                    temp = 0
                value = value + temp
                cf = cf + 1
                x_ijf = x_ijf + 1
        position_label.append(value)

    #step_ijf = step_ijf + 321
    #positions.append(position_label)
        
    #print (positions)
    return (position_label)

#position - 1
#print(computeNodePotentialByWord(0, 1))


# In[17]:

#computes transition potential
def computeTransitionPotentialByWordMod(j, i):
    param = open('/Users/anahita/Desktop/transition-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)
    
    transition_position = []
    #for j in range(len(word) - 1): 
    for c1 in range (10):
        for c2 in range(10):
            w = float(parameters[c1 * 10 + c2])
            #if(c1 == 1 and c2 == 1):
            #    print(w)
            transition_position.append(w)
        
    return transition_position

#print("salam")
#computeTransitionPotentialByWordMod(0, 1)


# In[18]:

#note that it is being computed for the first test word only
#computes clique potential j (or j+1 according to the algorithm)
def computeCliquePotential(j):
    node_potential = computeNodePotentialByWord(j, 1)
    
    transition_potential = computeTransitionPotentialByWordMod(j, 1)
    
    another_node_potential = computeNodePotentialByWord(j + 1, 1)
        
    
    for ij in range(10):
        for ijp in range(10):
            transition_potential[ij * 10 + ijp] = transition_potential[ij * 10 + ijp] + node_potential[ij]
            
    if(j == 2):
        for sj in range(10):
            for sjp in range(10):
                transition_potential[sj * 10 + sjp] = transition_potential[sj * 10 + sjp] + another_node_potential[sjp]
    
    return (transition_potential)

#prints the clique potential j
def printCliquePotential(j):
    clique_potential = computeCliquePotential(j)
    
    for c1 in range(10):
        for c2 in range(10):
            print("Y3= " + str(c1) + " Y4= " + str(c2) + " value= " + str(clique_potential[c1 * 10 + c2]))
            
#printCliquePotential(2)


# In[19]:

import math
#implementation of the logsumexp given in the lecture notes
def computeLogSumExp(vector):
    maximum = vector[0]
    for i in range(1, len(vector)):
        if(vector[i] > maximum):
            maximum = vector[i]
    
    for i in range(len(vector)):
        vector[i] = vector[i] - maximum
    
    summation = 0
    for i in range(len(vector)):
        summation = summation + math.exp(vector[i])
    
    return (maximum + math.log(summation)) 

#computes message from one to two
def computeMessageOneToTwo():
    clique_potential = computeCliquePotential(0)
    
    result = []
    for y2 in range(10):
        #summation = []
        summation = 0
        for y1 in range(10):
            #summation.append(clique_potential[(y1 * 10) + y2])
            summation = summation + math.exp(clique_potential[y1 * 10 + y2])
        #result.append(computeLogSumExp(summation))
        result.append(math.log(summation))
        
    return result

print(computeMessageOneToTwo())


# In[20]:

import math
#computes message three to two
def computeMessageThreeToTwo():
    clique_potential = computeCliquePotential(2)
    
    result = []
    for y3 in range(10):
        summation = []
        for y4 in range(10):
            summation.append(clique_potential[(y3 * 10) + y4])
        result.append(computeLogSumExp(summation)) 
            
    return result

result = computeMessageThreeToTwo()
for y3 in range(10):
    print("Y3= " + str(y3) + " value= " + str(result[y3]))


# In[21]:

import math
#computes message two to one
def computeMessageTwoToOne():
    clique_potential = computeCliquePotential(1)
    #clique_potential = [4, 2, 2, 1]
    message_three_two = computeMessageThreeToTwo()
    #message_three_two = [3, 3]
    
    result = []
    for y2 in range(10):
        summation = []
        for y3 in range(10):
            summation.append(clique_potential[(y2 * 10) + y3] + message_three_two[y3])
        result.append(computeLogSumExp(summation)) 
            
    return result

result = computeMessageTwoToOne()
for y2 in range(10):
    print("Y2= " + str(y2) + " value= " + str(result[y2]))


# In[22]:

import math
#computes message two to three
def computeMessageTwoToThree():
    clique_potential = computeCliquePotential(1)
    
    message_one_two = computeMessageOneToTwo()
    
    result = []
    for y3 in range(10):
        summation = []
        for y2 in range(10):
            summation.append(clique_potential[(y2 * 10) + y3] + message_one_two[y2])
        result.append(computeLogSumExp(summation))
        
    return result

result = computeMessageTwoToThree()
for y3 in range(10):
    print("Y3= " + str(y3) + " value= " + str(result[y3]))


# In[23]:

#computes belief i = 1
def computeBeliefOne():
    clique_potential = computeCliquePotential(0)
    
    #y2
    message_two_one = computeMessageTwoToOne()
    
    result = []
    for y1 in range(10):
        for y2 in range(10):
            temp = clique_potential[y1 * 10 + y2] + message_two_one[y2]
            result.append(temp)
        
    return result

result = computeBeliefOne()
for y1 in range(10):
    for y2 in range(10):
        print("Y1= " + str(y1) + " Y2= " + str(y2) + " value= " + str(result[y1 * 10 + y2]))


# In[24]:

#computes belief i = 2
def computeBeliefTwo(): 
    clique_potential = computeCliquePotential(1)
    
    #y2
    message_one_two = computeMessageOneToTwo()
    
    #y3
    message_three_two = computeMessageThreeToTwo()
    
    result = []
    for y2 in range(10):
        for y3 in range(10):
            temp = clique_potential[y2 * 10 + y3] + message_one_two[y2] + message_three_two[y3]
            result.append(temp)
        
    return result

result = computeBeliefTwo()
for y2 in range(10):
    for y3 in range(10):
        print("Y2= " + str(y2) + " Y3= " + str(y3) + " value= " + str(result[y2 * 10 + y3]))


# In[25]:

#computes belief i = 3
def computeBeliefThree():
    clique_potential = computeCliquePotential(2)
    
    #y3
    message_two_three = computeMessageTwoToThree()
    
    result = []
    for y3 in range(10):
        for y4 in range(10):
            temp = clique_potential[y3 * 10 + y4] + message_two_three[y3]
            result.append(temp)
        
    return result

result = computeBeliefThree()
for y3 in range(10):
    for y4 in range(10):
        print("Y3= " + str(y3) + " Y4= " + str(y4) + " value= " + str(result[y3 * 10 + y4]))


# In[26]:

import math
#computes pairwise marginal probability (Y1, Y2)
def computeCliqueOneProbability():
    belief_one = computeBeliefOne()
    
    second_part = 0
    for y1 in range(10):
        for y2 in range(10):
            second_part = second_part + math.exp(belief_one[y1 * 10 + y2])
        
        
    ##joint probability Y1, Y2
    joint_probability = []
    for y1 in range(10):
        for y2 in range(10):
            joint_probability.append(math.exp(belief_one[y1 * 10 + y2]) / second_part)
            #print(belief_one[y1 * 10 + y2] - second_part)
            
    #for y1 in range(10):
    #    for y2 in range(10):
    #        print("y1= " + str(y1) + " y2= " + str(y2) + " probability= " + str(joint_probability[y1 * 10 + y2]))
        
    ##p(y1)
    
    ##print(math.log())
        
    return joint_probability

result = computeCliqueOneProbability()
for y1 in range(10):
    for y2 in range(10):
        print("Y1= " + str(y1) + " Y2= " + str(y2) + " Probability= " + str(result[y1 * 10 + y2]))
#marginals for y1
marginals = []
for y1 in range(10):
    summation = 0
    for y2 in range(10):
        summation = summation + result[y1 * 10 + y2]
    marginals.append(summation)
for y1 in range(10):
    print("Y1= " + str(y1) + " Probability= " + str(marginals[y1]))
    
#marginals for y2
marginals2 = []
for y2 in range(10):
    summation = 0
    for y1 in range(10):
        summation = summation + result[y1 * 10 + y2]
    marginals2.append(summation)
for y2 in range(10):
    print("Y2= " + str(y2) + " Probability= " + str(marginals2[y2]))


# In[27]:

import math
#computes pairwise marginal probability (Y3, Y4)
def computeCliqueThreeProbability():
    belief_three = computeBeliefThree()
    
    second_part = 0
    for y3 in range(10):
        for y4 in range(10):
            second_part = second_part + math.exp(belief_three[y3 * 10 + y4])
        
        
    ##joint probability Y3, Y4
    joint_probability = []
    for y3 in range(10):
        for y4 in range(10):
            joint_probability.append(math.exp(belief_three[y3 * 10 + y4]) / second_part)
            #print(belief_one[y1 * 10 + y2] - second_part)
        
    return joint_probability

result = computeCliqueThreeProbability()
for y3 in range(10):
    for y4 in range(10):
        print("Y3= " + str(y3) + " Y4= " + str(y4) + " Probability= " + str(result[y3 * 10 + y4]))

#marginals for y3
marginals = []
for y3 in range(10):
    summation = 0
    for y4 in range(10):
        summation = summation + result[y3 * 10 + y4]
    marginals.append(summation)
for y3 in range(10):
    print("Y3= " + str(y3) + " Probability= " + str(marginals[y3]))
    
#marginals for y4
marginals2 = []
for y4 in range(10):
    summation = 0
    for y3 in range(10):
        summation = summation + result[y3 * 10 + y4]
    marginals2.append(summation)
for y4 in range(10):
    print("Y4= " + str(y4) + " Probability= " + str(marginals2[y4]))


# In[ ]:




# In[ ]:




# In[ ]:



