
# coding: utf-8

# In[52]:

##BLOCKS OF CODE FOR THE QUESTION 2
def getLabelPos (word, j):
    c = word[j]
    
    labels = "etainoshrd"
    for index in range(len(labels)):
        if(c == labels[index]):
            return index
    return -1

#computes node potential for the position j of the test image i
def computeNodePotentialByWord(j, i):
    param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)

    feature     = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)

    ##step_ijf = 0
    #positions = []
    step_ijf = 0
    for index in range(j):
        step_ijf = step_ijf + 321
    ##for j in range(len(word)):
    position_label = []
    for yj in range(10):
        cf  = 0
        value = 0
        for c in range(10):
            x_ijf = step_ijf
            for f in range(0, 321):
                # Wcf * xijf
                temp = float(parameters[cf]) * float(observations[x_ijf])
                #QUESTION: Am I right?
                #if(getLabelByPos(word, j) != c):
                #    temp = 0
                if(yj != c):
                    temp = 0
                value = value + temp
                cf = cf + 1
                x_ijf = x_ijf + 1
        position_label.append(value)

    #step_ijf = step_ijf + 321
    #positions.append(position_label)
        
    #print (positions)
    return (position_label)

#position - 1
#print(computeNodePotentialByWord(0, 1))


# In[53]:

#computes the transition potential 
def computeTransitionPotentialByWordMod(j, i):
    param = open('/Users/anahita/Desktop/transition-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)
    
    transition_position = []
    #for j in range(len(word) - 1): 
    for c1 in range (10):
        for c2 in range(10):
            w = float(parameters[c1 * 10 + c2])
            #if(c1 == 1 and c2 == 1):
            #    print(w)
            transition_position.append(w)
        
    return transition_position

#print("salam")
#computeTransitionPotentialByWordMod(0, 1)


# In[54]:

#computes clique potential 
#note that it is being computed for the first test word only
def computeCliquePotential(j, word_length, word_number):
    node_potential = computeNodePotentialByWord(j, word_number)
    
    transition_potential = computeTransitionPotentialByWordMod(j, word_number)
    
    for ij in range(10):
        for ijp in range(10):
            transition_potential[ij * 10 + ijp] = transition_potential[ij * 10 + ijp] + node_potential[ij]
            
    if(j == (word_length - 2)):
        another_node_potential = computeNodePotentialByWord(j + 1, word_number)
        for sj in range(10):
            for sjp in range(10):
                transition_potential[sj * 10 + sjp] = transition_potential[sj * 10 + sjp] + another_node_potential[sjp]
    
    return (transition_potential)

#prints clique potential
def printCliquePotential(j, word_length, word_number):
    clique_potential = computeCliquePotential(j, word_length, word_number)
    
    for c1 in range(10):
        for c2 in range(10):
            print("Y3= " + str(c1) + " Y4= " + str(c2) + " value= " + str(clique_potential[c1 * 10 + c2]))

#computeCliquePotential(2, 4, 1)


# In[55]:

import math
#implementation of the logsumexp algorithm presented in the lecture notes
def computeLogSumExp(vector):
    maximum = vector[0]
    for i in range(1, len(vector)):
        if(vector[i] > maximum):
            maximum = vector[i]
    
    for i in range(len(vector)):
        vector[i] = vector[i] - maximum
    
    summation = 0
    for i in range(len(vector)):
        summation = summation + math.exp(vector[i])
    
    return (maximum + math.log(summation)) 

#computes backward message from i to j for the word whose length is word_length and is the word_number th word in the 
#test_words.txt 
def computeMessageBackward(i, j, word_number, word_length):
    
    clique_potential = computeCliquePotential(i - 1, word_length, word_number)
    
    result = []
    if(i == (word_length - 1)):
        #print("salam")
        for ym in range(10):
            summation = 0
            for yn in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn])
            result.append(math.log(summation))
    else:
        message = computeMessageBackward(i + 1, i, word_number, word_length)
        for ym in range(10):
            summation = 0
            for yn in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn] + message[yn])
            result.append(math.log(summation))
    return result

#print(computeMessageBackward(2, 1, 1))


# In[56]:

#computes forward messages (from i to j) for the word in word_number th row of the test_words.txt 
#and whose length is word_length
def computeMessageForward(i, j, word_number, word_length):
    #y           = open('/Users/anahita/Desktop/test_words.txt')
    #word = ''
    #for l in range(word_number):
    #    word = y.readline()
    #word_length = len(word) - 1
    
    clique_potential = computeCliquePotential(i - 1, word_length, word_number)
    
    result = []
    if(i == 1):
        #print("salam")
        for yn in range(10):
            summation = 0
            for ym in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn])
            result.append(math.log(summation))
    else:
        message = computeMessageForward(i - 1, i, word_number, word_length)
        for yn in range(10):
            summation = 0
            for ym in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn] + message[ym])
            result.append(math.log(summation))
    return result

#print(computeMessageForward(2, 3, 1))


# In[57]:

#computes belief i for the word in row = word_number of the test_words.txt (length of the word in word_length)
def computeBelief(i, word_length, word_number):
    y           = open('/Users/anahita/Desktop/test_words.txt')
    word = ''
    for l in range(word_number):
        word = y.readline()
    word_length = len(word) - 1
    
    clique_potential = computeCliquePotential(i - 1, word_length, word_number)
    
    message = []
    message_m = []
    belief = []
    if(i == 1):
        message = computeMessageBackward(i + 1, i, word_number, word_length)
        for ym in range(10):
            for yn in range(10):
                belief.append(clique_potential[ym * 10 + yn] + message[yn])
    if(i == (word_length - 1)):
        message = computeMessageForward(i - 1, i, word_number, word_length)
        for ym in range(10):
            for yn in range(10):
                belief.append(clique_potential[ym * 10 + yn] + message[yn])
    if((i != 1) and (i != (word_length - 1))):
        message = computeMessageForward(i - 1, i, word_number, word_length)
        message_m = computeMessageBackward(i + 1, i, word_number, word_length)
        for ym in range(10):
            for yn in range(10):
                belief.append(clique_potential[ym * 10 + yn] + message[ym] + message[yn])
    
    return belief

#computeBelief(1, 4, 1)


# In[58]:

#position = 0
import math
#computes the most likely character for the given position j of row = word_number in the test_words.txt
def computeMostLikelyProbability(j, word_number):
    y           = open('/Users/anahita/Desktop/test_words.txt')
    word = ''
    for l in range(word_number):
        word = y.readline()
    word_length = len(word) - 1
    
    belief = []
    if(j != (word_length - 1)):
        belief = computeBelief(j + 1, word_length, word_number)
    else:
        belief = computeBelief(j, word_length, word_number)
    
    second_part = 0
    for ym in range(10):
        for yn in range(10):
            second_part = second_part + math.exp(belief[ym * 10 + yn])
            
    joint_prob = []
    for ym in range(10):
        for yn in range(10):
            joint_prob.append(math.exp(belief[ym * 10 + yn]) / second_part)
           
    result = []
    if(j != (word_length - 1)):
        for ym in range(10):
            summation = 0
            for yn in range(10):
                summation = summation + joint_prob[ym * 10 + yn]
            result.append(summation)
    else:
        for yn in range(10):
            summation = 0
            for ym in range(10):
                summation = summation + joint_prob[ym * 10 + yn]
            result.append(summation)
            
    maximum = result[0]
    index = 0
    for i in range(1, len(result)):
        if( result[i] > maximum):
            maximum = result[i]
            index = i
            
    #print("probability= " + str(maximum) + " character= " + str(index))

    return index


# In[59]:

def getLabel(c):
    if(c == 0):
        return 'e'
    if(c == 1):
        return 't'
    if(c == 2):
        return 'a'
    if(c == 3):
        return 'i'
    if(c == 4):
        return 'n'
    if(c == 5):
        return 'o'
    if(c == 6):
        return 's'
    if(c == 7):
        return 'h'
    if(c == 8):
        return 'r'
    if(c == 9):
        return 'd'
    return -1

#computes the average character-level accuracy
def computeAccuracy():
    total = 0
    accurate_predictions = 0
    y           = open('/Users/anahita/Desktop/test_words.txt')
    
    for i in range(1, 201):
        word = y.readline()
        word_length = len(word) - 1
        word_number = i
        total = total + word_length
        
        result = []
        for j in range(word_length):
            result.append(computeMostLikelyProbability(j, word_number))
            
        for j in range(word_length):
            if(getLabel(result[j]) == word[j]):
                accurate_predictions = accurate_predictions + 1
                print("me")
                
    return (accurate_predictions / total)

print(computeAccuracy())


# In[ ]:

972 

