
# coding: utf-8

# In[6]:

from scipy.optimize import fmin_bfgs
import scipy

def fitness(p):
    return  (1 - p[0]) * (1 - p[0]) + 100 * (p[1] - p[0] * p[0]) * (p[1] - p[0] * p[0])

def fitness_der(p):
    return scipy.array((-2 * (1 - p[0]) + 400 * p[0] * p[0] * p[0] - 400 * p[0] * p[1], 200 * p[1] - 200 * p[0] * p[0]))

p = [0.0, 0.0]
popt = fmin_bfgs(fitness, p, fprime = fitness_der)
print (popt)


# In[ ]:



