
# coding: utf-8

# In[1]:

##QUESTION 1.1
#computes node potential for ith test word
def computeNodePotential(i):
    param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)

    feature     = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)
    
    y           = open('/Users/anahita/Desktop/test_words.txt')
    word = ''
    for l in range(i):
        word = y.readline()

    ##step_ijf = 0
    positions = []
    step_ijf = 0
    for j in range(len(word) - 1):
        position_label = []
        for y_ij in range(10):
            cf  = 0
            value = 0
            for c in range(10):
                x_ijf = step_ijf
                for f in range(0, 321):
                    # Wcf * xijf
                    temp = float(parameters[cf]) * float(observations[x_ijf])
                    #QUESTION: Am I right?
                    #if(getLabelByPos(word, j) != c):
                    #    temp = 0
                    if(y_ij != c):
                        temp = 0
                    value = value + temp
                    cf = cf + 1
                    x_ijf = x_ijf + 1
            position_label.append(value)

        step_ijf = step_ijf + 321
        positions.append(position_label)
        
    #print (positions)
    return (positions)
    
#prints node potential
def printNodePotential(i):
    result = computeNodePotential(i)
    print(len(result))
    for j in range(len(result)):
        print("****Position=" + str(j) + "\n")
        for yij in range(10):
            print (" label= " + str(yij) + " value=" + repr(result[j][yij]))
            
printNodePotential(1)


# In[2]:

##Question 1.2
def getLabelPos (word, j):
    c = word[j]
    
    labels = "etainoshrd"
    for index in range(len(labels)):
        if(c == labels[index]):
            return index
    return -1

#returns a vector with len(word) columns, each represents potential value for each position
#computes node potential for the ith word given "word"
def computeNodePotentialByWord(i, word):
    param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)

    feature     = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)

    ##step_ijf = 0
    positions = []
    step_ijf = 0
    #len(word) -1 because of \n
    for j in range(len(word) -1):
        cf  = 0
        value = 0
        for c in range(10):
            x_ijf = step_ijf
            for f in range(0, 321):
                # Wcf * xijf
                temp = float(parameters[cf]) * float(observations[x_ijf])
                if(getLabelPos(word, j) != c):
                    temp = 0
                value = value + temp
                cf = cf + 1
                x_ijf = x_ijf + 1
        positions.append(value)
        step_ijf = step_ijf + 321
    #print (positions)
    return (positions)

#computeNodePotentialByWord(1, "sala")


# In[3]:

#returns a vector with len(word - 1) columns, each represents transition parameter for the corresponding sequence of labels
#computes transition potential
def computeTransitionPotentialByWord(i, word):
    param = open('/Users/anahita/Desktop/transition-params.txt', 'r')
    params = param.read()
    parameters = params.split()
    #print (parameters)
    
    transition_position = []
    for j in range(len(word) - 1): 
        w = float(parameters[getLabelPos(word, j) * 10 + getLabelPos(word, j + 1)])
        transition_position.append(w)
        
    return transition_position

computeTransitionPotentialByWord(1, "that")


# In[4]:

#computes energy for the true label sequence for the ith test word
def computeEnergyForTrueLabelSeq(i):
    #we want to compute the value for the first three test words
    file = open('/Users/anahita/Desktop/test_img' + str(i) + '.txt')
    
    y     = open('/Users/anahita/Desktop/test_words.txt')
    word = ''
    for l in range(i):
        word = y.readline()
        
    #first summation term
    node_positions = computeNodePotentialByWord(i, word)
    term1 = node_positions[0]
    #compute \sum \limits_{j = 1}^{L_{i}}
    for j in range(1, len(word) - 1):
        term1 = term1 + node_positions[j]
    print("term1")
    print (term1)
    
    
    #second summation term
    transition_positions = computeTransitionPotentialByWord(i, word)
    term2 = transition_positions[0]
    #compute \sum \limits_{j = 1}^{L_{i} - 1}
    for j in range(1, len(word) - 2): 
        term2 = term2 + transition_positions[j]
    print ("term2")
    print (term2)
    
    return (term1 + term2)

print ("Energy value for the first test word= " + str(computeEnergyForTrueLabelSeq(1)))
print ("\n")
print ("Energy value for the second test word= " + str(computeEnergyForTrueLabelSeq(2)))
print ("\n")
print ("Energy value for the third test word= " + str(computeEnergyForTrueLabelSeq(3)))


# In[ ]:




# In[ ]:




# In[ ]:




# In[ ]:




# In[ ]:




# In[ ]:



