
# coding: utf-8

# In[62]:

#IMPORTED NORM1.PY
import numpy as np
import math
def truncnormal(mean, sd, Lbound,Rbound):
    """
    sample from truncated normal, trunctated to range [L,R]
    NOTE this takes STD DEV (**not** variance!)
    use L=0, R=100 to pretty-good approximate [0,+inf)
    pg21 of https://www2.stat.duke.edu/courses/Fall03/sta216/lecture10.pdf
    """
    assert Rbound >= Lbound
    Lq,Rq = phi((Lbound-mean)/sd), phi((Rbound-mean)/sd)
    if Lq==Rq:
        # The mean is more than 9 or so stdevs away from the truncation area.
        if mean < Lbound: return Lbound
        elif mean > Rbound: return Rbound 
        else: assert False, "wtf but we are within the bounds..."
    r = np.random.random() * (Rq-Lq) + Lq
    return mean+sd*normal_CDF_inverse(r)

def normcdf(x, mean=0.0, sd=1.0):
    return phi( (x-mean)/sd )
 
def phi(x):
    """CDF for N(0,1)"""
    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # Save the sign of x
    sign = 1
    if x < 0:
        sign = -1
    x = abs(x)/math.sqrt(2.0)

    # A&S formula 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)

    return 0.5*(1.0 + sign*y)
 
def rational_approximation(t):
    # Abramowitz and Stegun formula 26.2.23.
    # The absolute value of the error should be less than 4.5 e-4.
    c = [2.515517, 0.802853, 0.010328]
    d = [1.432788, 0.189269, 0.001308]
    numerator = (c[2]*t + c[1])*t + c[0]
    denominator = ((d[2]*t + d[1])*t + d[0])*t + 1.0
    return t - numerator / denominator
 
def normal_CDF_inverse(p):
    assert p > 0.0 and p < 1
 
    if p < 0.5:
        # F^-1(p) = - G^-1(p)
        return -rational_approximation( math.sqrt(-2.0*math.log(p)) )
    else:
        # F^-1(p) = G^-1(1-p)
        return rational_approximation( math.sqrt(-2.0*math.log(1.0-p)) )


# In[63]:

"""
Example starter code for Bayesian linear regression implementations.
"""
from __future__ import division
import numpy as np
import scipy.stats
from numpy.linalg import inv 

def linreg_post(X,Y, priormean, priorvar_scalar, emissionvar_scalar):
    # returns (PosteriorMean, PosteriorCovar)
    # where PosteriorMean is a J-length vector
    # where PosteriorCovar is a (J x J) shaped matrix
    #N = X.shape[0]
    #J = X.shape[1]
    #assert N==len(Y)
    
    #V0 is the prior covariance of the weights
    #emmissionvar_scalar is the sigma^2
    #W0 corresponds to the prior mean of the weights
    
    #VN = σ2(σ2V−10 + XTX)−1
    temp = (emissionvar_scalar * inv(priorvar_scalar)) + (np.dot(np.transpose(X), X))
    V_N = emissionvar_scalar * inv(temp)
    #print("V_N" + str(V_N))
    
    #WN = VNV−10 w0 +1σ2VNXT y
    temp1 = np.dot(V_N, inv(priorvar_scalar))
    temp2 = np.dot(temp1, priormean)
    temp3 = (1/emissionvar_scalar) * V_N
    temp4 = np.dot(temp3, np.transpose(X))
    W_N = np.dot(temp4, Y)
    #print("W_N" + str(W_N))
    
    result = []
    result.append(W_N)
    result.append(V_N)
    
    return result


# In[99]:

from __future__ import division
import numpy as np
import sys,os
from datetime import datetime
import matplotlib.pyplot as plt
import matplotlib.pyplot as plt2
import math as ma
##import norm1d
#DO NOT FORGET TO ADD IT!
##import linreg;reload(linreg)

## Load the data and delete the two metadata columns
votes=np.loadtxt("/Users/anahita/Desktop/allvotes.txt",dtype=int)
votes=votes[:,2:]
#print(votes.shape)

Njus = votes.shape[1]
Ncase= votes.shape[0]
jusnames = [x.strip() for x in open("/Users/anahita/Desktop/justicenames.txt")]
assert len(jusnames)==Njus

def GibbsSampler():
    #GENERATION PROCESS FOR ALPHA AND BETA
    alpha = np.ndarray(shape=(Ncase, 1), dtype=float)
    beta  = np.ndarray(shape=(Ncase, 1), dtype=float)
    for k in range(Ncase):
        #HOW CAN I SET b_{0} = 0 and B_{0} = I_2?
        alpha_beta = np.random.multivariate_normal([0, 0], np.identity(2))
        alpha[k] = alpha_beta[0]
        beta[k]  = alpha_beta[1]
        
    print("done_alpha_beta")
    
    #GENERATION PROCESS FOR THETA
    theta = np.ndarray(shape=(Njus, 1), dtype=float)
    for j in range(Njus):
        #HOW CAN I SENT m_{j} and C_{j}?
        theta[j] = np.random.normal(0, 1)
    print("done_theta")
        
    #GENERATION PROCESS FOR Z
    z = np.ndarray(shape=(Ncase, Njus), dtype=float)
    for k in range(Ncase):
        for j in range(Njus):
            if(votes[k, j] == 1 or votes[k, j] == 0):
                error = np.random.normal(0, 1)
                z[k, j] = alpha[k] + beta[k] * theta[j] + error   
    print("done_z")
    
    print("generative process has been done!")

    #SAMPLING PROCESS
    #IT SHOULD BE SET TO 1000
    samples = []
    posterior_mean = []
    posterior_var = []
    iter_num = 1000
    for siter in range(iter_num):
        print ("ITER" + str(siter))
        
        #RESAMPLE z
        for k in range(Ncase):
            for j in range(Njus):
                #print(votes[k, j])
                if(votes[k, j] == 1):
                    z[k, j] = truncnormal(alpha[k] + (beta[k] * theta[j]), 1, 0, 100)
                if(votes[k, j] == 0):
                    z[k, j] = truncnormal(alpha[k] + (beta[k] * theta[j]), 1, -100, 0)
        
        print("resampled z")
        
        #RESAMPLE theta
        pos_mean = []
        pos_var = []
        for j in range(Njus):
            counter = 0
            for k in range(Ncase):
                if(votes[k, j] == 0 or votes[k, j] == 1):
                    counter = counter + 1
            
            z_sum = np.ndarray(shape=(counter, 1), dtype=float)
            beta_sum = np.ndarray(shape=(counter, 1), dtype=float)
            counter = 0
            for k in range(Ncase):
                if(votes[k, j] == 0 or votes[k, j] == 1):
                    z_sum[counter, 0] = z[k, j] - alpha[k]
                    beta_sum[counter, 0] = beta[k]
                    counter = counter + 1
                
            mean_covar = linreg_post(beta_sum, z_sum, [0], np.array([[1]]), 1)
            #theta[j] = np.random.normal(mean_covar[0], ma.sqrt(mean_covar[1]))
            theta[j] = mean_covar[0] + np.random.randn() * ma.sqrt(mean_covar[1])
            pos_mean.append(mean_covar[0])
            pos_var.append(ma.sqrt(mean_covar[1]))
            #print(theta[j])
        posterior_mean.append(pos_mean)
        print(pos_mean)
        posterior_var.append(pos_var)
        print("resampled theta")
        
        #RESAMPLE alpha and beta
        for k in range(Ncase):
            counter = 0
            for j in range(Njus):
                if(votes[k, j] == 1 or votes[k, j] == 0):
                    counter = counter + 1
            z_sum = np.ndarray(shape=(counter, 1), dtype=float)
            theta_sum = np.ndarray(shape=(counter, 2), dtype=float)
            counter = 0
            for j in range(Njus):
                if(votes[k, j] == 1 or votes[k, j] == 0):
                    z_sum[counter, 0] = z[k, j] 
                    theta_sum[counter, 0] = 1
                    theta_sum[counter, 1] = theta[j]
                    counter = counter + 1
            ab_priormean = [0, 0]
            ab_priorvar = np.identity(2)
            mean_covar = linreg_post(theta_sum, z_sum, ab_priormean, ab_priorvar, 1)
            mean  = mean_covar[0]
            m = []
            m.append(mean[0, 0])
            m.append(mean[1, 0])
            alpha_beta = np.random.multivariate_normal(m, mean_covar[1])
            alpha[k] = alpha_beta[0]
            beta[k]  = alpha_beta[1]
        
        #print(alpha[12])
        print("resampled alpha beta")
        
        #print(alpha[12])
        
        #SAVE DATA
        samples.append({'theta':theta.copy(), 'alpha':alpha.copy(), 'beta':beta.copy()})
    
    #for i in range(100):
        #print("alpha" + str(samples[i]['alpha'][15]))
        #print("beta"  + str(samples[i]['beta'][15]))
    x_axis = []
    for i in range(iter_num):
        x_axis.append(i)
        
    #draw theta j
    
    plt.clf()
    for j in range(5):
        y_axis = []
        for iteration in range(iter_num):
        #pos_mean = posterior_mean[iteration]
            y_axis.append(samples[iteration]['theta'][j])
        plt.plot(x_axis, y_axis)
        
    plt.savefig('/Users/anahita/Desktop/theta-0-4-1000m.pdf')
    
    
    plt.clf()
    for j in range(5, 10):
        y_axis = []
        for iteration in range(iter_num):
            #pos_mean = posterior_mean[iteration]
            y_axis.append(samples[iteration]['theta'][j])
        plt.plot(x_axis, y_axis)
        
    plt.savefig('/Users/anahita/Desktop/theta-5-9-1000m.pdf')
    
    plt.clf()
    for j in range(10, 15):
        y_axis = []
        for iteration in range(iter_num):
        #pos_mean = posterior_mean[iteration]
            y_axis.append(samples[iteration]['theta'][j])
        plt.plot(x_axis, y_axis)
        
    plt.savefig('/Users/anahita/Desktop/theta-10-14-1000m.pdf')
    
    
    plt.clf()
    for j in range(15, 20):
        y_axis = []
        for iteration in range(iter_num):
        #pos_mean = posterior_mean[iteration]
            y_axis.append(samples[iteration]['theta'][j])
        plt.plot(x_axis, y_axis)
        
    plt.savefig('/Users/anahita/Desktop/theta-15-19-1000m.pdf')
    
    plt.clf()
    for j in range(20, 25):
        y_axis = []
        for iteration in range(iter_num):
        #pos_mean = posterior_mean[iteration]
            y_axis.append(samples[iteration]['theta'][j])
        plt.plot(x_axis, y_axis)
        
    plt.savefig('/Users/anahita/Desktop/theta-20-24-1000m.pdf')
    
    plt.clf()
    for j in range(25, 29):
        y_axis = []
        for iteration in range(iter_num):
        #pos_mean = posterior_mean[iteration]
            y_axis.append(samples[iteration]['theta'][j])
        plt.plot(x_axis, y_axis)
        
    plt.savefig('/Users/anahita/Desktop/theta-25-28-1000m.pdf')
    #plt.show()
        
    x_axis = []
    for i in range(iter_num):
        x_axis.append(i)
    print("SALAM")
    for j in range(29):
        means = []
        sds = []
        for i in range(iter_num):
            mean = posterior_mean[i]
            sd  = posterior_var[i]
            #print(float(mean[j][0]))
            means.append(mean[j][0])
            sds.append(sd[j])
        plt.clf()
        plt.plot(x_axis, means)
        plt.plot(x_axis, sds)
        plt.savefig('/Users/anahita/Desktop/pos-mean-sd' + str(j) + '.pdf')
        
        
GibbsSampler()


# In[37]:

from numpy.linalg import inv
a = np.array([[1.]])
ainv = inv(a)
print(ainv)


# In[49]:

import math as ma
ma.sqrt(4.8)


# In[ ]:



