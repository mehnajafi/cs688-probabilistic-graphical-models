
# coding: utf-8

# In[53]:

import numpy as np
from numpy.linalg import inv 
#implementation of linear regression MLE
def linreg_mle(X,Y):
    # returns one vector of the inferred MLE weights.
    # X: matrix shape (N x J)
    # Y: vector length N
    #N = X.shape[0]
    #J = X.shape[1]
    #assert N==len(Y)
    
    #compute X'
    x_trans = np.transpose(X)
    
    #compute X'X
    im_result = np.dot(x_trans, X)
    
    #compute inv((X' X))
    im_result_inv = inv(im_result)        
    
    #inv((X'X)) X'
    im_result2 = np.dot(im_result_inv, x_trans)
    
    #inv(X'X) X' Y
    return np.dot(im_result2, Y)


# In[54]:

import statsmodels.api as sm
import numpy as np
#checking correctness for 1.a
def check():
    #example to check 1.a
    #Y = [100,300]
    #X = [1500, 420]
    #X = sm.add_constant(X)
    X = [1, 0]
    X = sm.add_constant(X)
    print(X)
    Y = [0.2, -0.3]
    
    #print(X)
    model = sm.OLS(Y,X)
    results = model.fit()
    print(results.params)
    
    
    Y_arr = np.ndarray(shape=(2, 1), dtype=float)
    Y_arr[0, 0] = 0.2
    Y_arr[1, 0] = -0.3
            
    X_arr = np.ndarray(shape=(2, 2), dtype=float)
    X_arr[0, 0] = 1
    X_arr[1, 0] = 1
    X_arr[0, 1] = 1
    X_arr[1, 1] = 0
    #print(X_arr_res)
    
    print(linreg_mle(X_arr, Y_arr))
check()


# In[55]:

"""
Example starter code for Bayesian linear regression implementations.
"""
from __future__ import division
import numpy as np
import scipy.stats
from numpy.linalg import inv 

#implementation of Bayesian linear regression
def linreg_post(X,Y, priormean, priorvar_scalar, emissionvar_scalar):
    # returns (PosteriorMean, PosteriorCovar)
    # where PosteriorMean is a J-length vector
    # where PosteriorCovar is a (J x J) shaped matrix
    #N = X.shape[0]
    #J = X.shape[1]
    #assert N==len(Y)
    
    #V0 is the prior covariance of the weights
    #emmissionvar_scalar is the sigma^2
    #W0 corresponds to the prior mean of the weights
    
    #VN = σ2(σ2V−10 + XTX)−1
    temp = (emissionvar_scalar * inv(priorvar_scalar)) + (np.dot(np.transpose(X), X))
    V_N = emissionvar_scalar * inv(temp)
    #print("V_N" + str(V_N))
    
    #WN = VNV−10 w0 +1σ2VNXT y
    temp1 = np.dot(V_N, inv(priorvar_scalar))
    temp2 = np.dot(temp1, priormean)
    temp3 = (1/emissionvar_scalar) * V_N
    temp4 = np.dot(temp3, np.transpose(X))
    W_N = np.dot(temp4, Y)
    #print("W_N" + str(W_N))
    
    result = []
    result.append(W_N)
    result.append(V_N)
    
    return result


# In[56]:

import numpy as np
from numpy.linalg import inv

#checking correctness for 1.b
def check_2b():
    #check1: nearly the same as the MLE

    #compute 1.a
    X = [1, 0]
    X = sm.add_constant(X)
    Y = [0.2, -0.3]
    #Y = [100,300]
    #X = [1500, 420]
    
    Y_arr = np.ndarray(shape=(2, 1), dtype=float)
    Y_arr[0, 0] = 0.2
    Y_arr[1, 0] = -0.3
            
    X_arr = np.ndarray(shape=(2, 2), dtype=float)
    X_arr[0, 0] = 1
    X_arr[1, 0] = 1
    X_arr[0, 1] = 1
    X_arr[1, 1] = 0
    
    result1 = linreg_mle(X_arr, Y_arr)
    print(result1)
    
    #compute 1.b
    priormean = [0, 0]
    
    priorvar = 1000 * np.identity(2)
    
    result2 = linreg_post(X_arr, Y_arr, priormean, priorvar, 1)
    print(result2[0])
    
    #temp1 = inv(0.5 * np.identity(2) + np.dot(np.transpose(X_arr), X_arr))
    #temp2 = np.dot(temp1, np.transpose(X))
    #temp3 = np.dot(temp2, Y)
    #print(temp3)
    
check_2b()


# In[57]:

import math
def normcdf(x, mean=0.0, sd=1.0):
    #print("mean= " + str(mean))
    #print("var= " + str(sd))
    return phi( (x-mean)/sd )
 
def phi(x):
    """CDF for N(0,1)"""
    # constants
    a1 =  0.254829592
    a2 = -0.284496736
    a3 =  1.421413741
    a4 = -1.453152027
    a5 =  1.061405429
    p  =  0.3275911

    # Save the sign of x
    sign = 1
    if x < 0:
        sign = -1
    x = abs(x)/math.sqrt(2.0)

    # A&S formula 7.1.26
    t = 1.0/(1.0 + p*x)
    y = 1.0 - (((((a5*t + a4)*t) + a3)*t + a2)*t + a1)*t*math.exp(-x*x)

    return 0.5*(1.0 + sign*y)


# In[58]:

import numpy as np
import math as ma
#implementation of correct version of cook test
def cook_linreg(X, priormean, priorvar_scalar, emissionvar_scalar, Nsim):
    """Runs Nsim simulations and returns list length Nsim of results for each one.
    Each result might contain the posterior mean/covar calculation,
    and/or the marginal posterior CDF of the "true" weight variable you're
    interested in, etc.
    (obviously there are lots of alternative ways to structure this.)
    """
    #X = np.array([ [1,1], [1,2], [1,3] ])
    #N = X.shape[0]
    #J = X.shape[1]
    #assert J==len(priormean)
    # TODO
    
    result = []
    for num_sim in range(Nsim):
        im_result = []
        
        #STEP1: sample w with priormean and priorvar
        w = np.random.multivariate_normal(priormean, priorvar_scalar)
        #print("w= " + str(w))
        
        #sample y
        y = []
        for i in range(X.shape[0]):
            ws = []
            for n in range(1, len(w)):
                ws.append(w[n])
            #print("ws= " + str(ws))
            xs = []
            for n in range(1, X.shape[1]):
                xs.append(X[i, n])
            #print("xs= " + str(xs))
            #print("dot= " + str(np.dot(ws, xs)))
            y.append(np.random.normal(w[0] + np.dot(ws, xs), emissionvar_scalar))
            
        #STEP2: infer posterior
        posterior = linreg_post(X,y, priormean, priorvar_scalar, emissionvar_scalar)
        
        #STEP3: infer the CDF of the true simulated w1
        temp = posterior[0]
        #print("temp= " + str(temp))
        posterior_mean_w1 = temp[1]
        
        temp2 = posterior[1]
        posterior_sd_w1 = ma.sqrt(temp2[1, 1])
        
        cdf = normcdf(w[1], posterior_mean_w1, posterior_sd_w1)
        
        #im_result.append(posterior[0])
        #im_result.append(posterior[1])
        im_result.append(cdf)
        
        result.append(cdf)
        
        #check for w1
        #temp1 = posterior[1]
        #var_w1 = temp1[1, 1]
        
        #print("w1= " + str(w[1]))
        #print("interval= " + str(posterior_mean_w1 - 1.96 * ma.sqrt(var_w1)) + "," + str(posterior_mean_w1 + 1.96 * ma.sqrt(var_w1)))
    return result


# In[59]:

import statsmodels.api as sm
#checking the correctness of cook test
def check_cook():
    X = [1, 0]
    X = sm.add_constant(X)
    Y = [0.2, -0.3]
    #Y = [100,300]
    #X = [1500, 420]
    
    Y_arr = np.ndarray(shape=(2, 1), dtype=float)
    Y_arr[0, 0] = 0.2
    Y_arr[1, 0] = -0.3
            
    X_arr = np.ndarray(shape=(2, 2), dtype=float)
    X_arr[0, 0] = 1
    X_arr[1, 0] = 1
    X_arr[0, 1] = 1
    X_arr[1, 1] = 0
    
    priormean = [0, 0]
    
    priorvar = 2 * np.identity(2)
    
    #result = cook_linreg_buggy(X_arr, priormean, priorvar, 1, 10000)
    result = cook_linreg(X_arr, priormean, priorvar, 1, 10000)
    
    return result
#check_cook()


# In[60]:

import matplotlib.pyplot as plt2
#draws the decile histogram plot for the correct version
#to draw decile histogram plot for the buggy version, 
#we should call cook_linreg_buggy in the check_cook
def drawDecileHistogram():
    result = check_cook()
    
    cdf = []
    for i in range(10000):
        #print("result= " + str(i))
        cdf.append(result[i])
        
    #print("done")
    plt2.clf()
    plt2.hist(cdf)
    plt2.savefig('/Users/anahita/Desktop/decile-buggy-me.pdf')
    #plt.show()
drawDecileHistogram()


# In[61]:

import numpy as np
import math as ma
#implementation of buggy version of cook test
def cook_linreg_buggy(X, priormean, priorvar_scalar, emissionvar_scalar, Nsim):
    """Runs Nsim simulations and returns list length Nsim of results for each one.
    Each result might contain the posterior mean/covar calculation,
    and/or the marginal posterior CDF of the "true" weight variable you're
    interested in, etc.
    (obviously there are lots of alternative ways to structure this.)
    """
    #X = np.array([ [1,1], [1,2], [1,3] ])
    #N = X.shape[0]
    #J = X.shape[1]
    #assert J==len(priormean)
    # TODO
    
    result = []
    for num_sim in range(Nsim):
        im_result = []
        
        #STEP1: sample w with priormean and priorvar
        w = np.random.multivariate_normal(priormean, priorvar_scalar)
        #print("w= " + str(w))
        
        #sample y
        y = []
        for i in range(X.shape[0]):
            ws = []
            for n in range(1, len(w)):
                ws.append(w[n])
            #print("ws= " + str(ws))
            xs = []
            for n in range(1, X.shape[1]):
                xs.append(X[i, n])
            #print("xs= " + str(xs))
            #print("dot= " + str(np.dot(ws, xs)))
            y.append(np.random.normal(1 + np.dot(ws, xs), emissionvar_scalar))
            
        #STEP2: infer posterior
        posterior = linreg_post(X,y, priormean, priorvar_scalar, emissionvar_scalar)
        
        #STEP3: infer the CDF of the true simulated w1
        temp = posterior[0]
        #print("temp= " + str(temp))
        posterior_mean_w1 = temp[1]
        
        temp2 = posterior[1]
        posterior_sd_w1 = ma.sqrt(temp2[1, 1])
        
        cdf = normcdf(w[1], posterior_mean_w1, posterior_sd_w1)
        
        #im_result.append(posterior[0])
        #im_result.append(posterior[1])
        im_result.append(cdf)
        
        result.append(cdf)
        
        #check for w1
        #temp1 = posterior[1]
        #var_w1 = temp1[1, 1]
        
        #print("w1= " + str(w[1]))
        #print("interval= " + str(posterior_mean_w1 - 1.96 * ma.sqrt(var_w1)) + "," + str(posterior_mean_w1 + 1.96 * ma.sqrt(var_w1)))
    return result


# In[ ]:



