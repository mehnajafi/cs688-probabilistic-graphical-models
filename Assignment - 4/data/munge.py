"""
not needed for the assignment, just for documentation
munges the data release from martin&quinn
into the allvotes.txt format seen here.
"""

import numpy as np
import os,glob

# constant/ is from
# http://mqscores.berkeley.edu/replication.php
# http://mqscores.berkeley.edu/media/constant.tar.gz

njus = 29

adj = np.loadtxt("../constant/data/adj.txt", dtype=int)
for f in sorted(glob.glob("../constant/data/vote/????.txt")):
    year = os.path.basename(f).replace(".txt","")
    yearix = int(year) - 1953
    localix2justice = {loc:j for (j,loc) in enumerate(adj[yearix]) if loc != -9}
    # print year, localix2justice

    for casei,line in enumerate(open(f)):
        localvotes = [int(x) for x in line.split()]
        votes = [-9]*njus
        for i,j in localix2justice.items():
            votes[j] = localvotes[i]
        print "%s %s %s" % (year,casei, ' '.join(str(x) for x in votes))
