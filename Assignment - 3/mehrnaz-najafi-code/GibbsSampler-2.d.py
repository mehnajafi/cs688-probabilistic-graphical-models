
# coding: utf-8

# In[24]:

import math
#case1: i = 0  and j = 0
#two neighbours: (0, 1) and (1, 0)
def computeRegion1(y , i, j, x, wp, wl):
    first_term = math.exp(wp * y[0, 1] + wp * y[1, 0] + wl * x[0, 0])
    #print(first_term)
    second_term = math.exp(wp * (1 - y[0, 1]) + wp * (1 - y[1, 0]) + wl * (1 - x[0, 0]))
    #print(second_term)
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[25]:

import math
#case2: i = 49 and j = 0
#two neighbours: (48, 0) and (49, 1)
def computeRegion2(y, i, j, x, wp, wl):
    first_term = math.exp(wp * y[48, 0] + wp * y[49, 1] + wl * x[i, j])
    #print(first_term)
    second_term = math.exp(wp * (1 - y[48, 0]) + wp * (1 - y[49, 1]) + wl * (1 - x[i, j]))
    #print(second_term)
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[26]:

import math
#case3: i = 0 and j = 49
#two neighbours: (0, 48) and (1, 49)
def computeRegion3(y, i, j, x, wp, wl):
    first_term = math.exp(wp * y[0, 48] + wp * y[1, 49] + wl * x[i, j])
    #print(first_term)
    second_term = math.exp(wp * (1 - y[0, 48]) + wp * (1 - y[1, 49]) + wl * (1 - x[i, j]))
    #print(second_term)
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[27]:

import math
#case4: i = 49 and j = 49
#two neighbours: (48, 49) and (49, 48)
def computeRegion4(y, i, j, x, wp, wl):
    first_term = math.exp(wp * y[48, 49] + wp * y[49, 48] + wl * x[i, j])
    #print(first_term)
    second_term = math.exp(wp * (1 - y[48, 49]) + wp * (1 - y[49, 48]) + wl * (1 - x[i, j]))
    #print(second_term)
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[28]:

import math
#case5: j = 0
def computeRegion5(y, i, j, x, wp, wl):
    first_term = (wp * y[i - 1, j])
    first_term = first_term + (wp * y[i + 1, j])
    first_term = first_term + (wp * y[i, j + 1] + wl * x[i, j])
    first_term = math.exp(first_term)
    #y = 0
    second_term = (wp * (1 - y[i - 1, j]))
    second_term = second_term + (wp * (1 - y[i + 1, j]))
    second_term = second_term + (wp * (1 - y[i, j + 1]) + wl * (1 - x[i, j]))
    second_term = math.exp(second_term)
    #accomodate y = 1
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[29]:

import math
#case6: j = 49
def computeRegion6(y, i, j, x, wp, wl):
    first_term = (wp * y[i - 1, j])
    first_term = first_term + (wp * y[i + 1, j])
    first_term = first_term + (wp * y[i, j - 1] + wl * x[i, j])
    first_term = math.exp(first_term)
    #y = 0
    second_term = (wp * (1 - y[i - 1, j]))
    second_term = second_term + (wp * (1 - y[i + 1, j]))
    second_term = second_term + wp * (1 - y[i, j - 1]) + wl * (1 - x[i, j])
    second_term = math.exp(second_term)
    #accomodate y = 1
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[30]:

import math
#case7: i = 0
def computeRegion7(y, i, j, x, wp, wl):
    first_term = (wp * y[i, j - 1])
    first_term = first_term + (wp * y[i, j + 1])
    first_term = first_term + (wp * y[i + 1, j] + wl * x[i, j])
    first_term = math.exp(first_term)
    #y = 0
    second_term = wp * (1 - y[i, j - 1])
    second_term = second_term + (wp * (1 - y[i, j + 1]))
    second_term = second_term + (wp * (1 - y[i + 1, j]) + wl * (1 - x[i, j]))
    second_term = math.exp(second_term)
    #accomodate y = 1
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[31]:

import math
#case8: i = 49
def computeRegion8(y, i, j, x, wp, wl):
    first_term = (wp * y[i, j - 1])
    first_term = first_term + (wp * y[i, j + 1])
    first_term = first_term + (wp * y[i - 1, j] + wl * x[i, j])
    first_term = math.exp(first_term)
    #y = 0
    second_term = (wp * (1 - y[i, j - 1]))
    second_term = second_term + (wp * (1 - y[i, j + 1]))
    second_term = second_term + (wp * (1 - y[i - 1, j]) + wl * (1 - x[i, j]))
    second_term = math.exp(second_term)
    #accomodate y = 1
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[32]:

import math
#case9: otherwise
def computeRegion9(y, i, j, x, wp, wl):
    first_term = (wp * y[i, j - 1])
    first_term = first_term + (wp * y[i, j + 1])
    first_term = first_term + (wp * y[(i + 1), j])
    first_term = first_term + (wp * y[(i - 1), j] + wl * x[i, j])
    first_term = math.exp(first_term)
    #y = 0
    second_term = (wp * (1 - y[i, j - 1]))
    second_term = second_term + (wp * (1 - y[i, j + 1]))
    second_term = second_term + (wp * (1 - y[(i + 1), j]))
    second_term = second_term + (wp * (1 - y[(i - 1), j]) + wl * (1 - x[i, j]))
    second_term = math.exp(second_term)
    #accomodate y = 1
    second_term = second_term + first_term
    #print(second_term)
    return (first_term / second_term)


# In[33]:

import math
import numpy as np
def computeConditionalDistribution(y, i, j, x, wp, wl):
    if ((j == 0) or (j == 49)):
        if((i == 0) and (j == 0)):
            return computeRegion1(y, i, j, x, wp, wl)
        if((i == 49) and (j == 0)):
            return computeRegion2(y, i, j, x, wp, wl)
        if((i == 0) and (j == 49)):
            return computeRegion3(y, i, j, x, wp, wl)
        if((i == 49) and (j == 49)):
            return computeRegion4(y, i, j, x, wp, wl)
        if(j == 0):
            return computeRegion5(y, i, j, x, wp, wl)
        if(j == 49):
            return computeRegion6(y, i, j, x, wp, wl)
    if(i == 0):
        return computeRegion7(y, i, j, x, wp, wl)
    if(i == 49):
        return computeRegion8(y, i, j, x, wp, wl)
    return computeRegion9(y, i, j, x, wp, wl)


# In[34]:

import random
import matplotlib.pyplot as plt
import numpy as np
def sweep(y, x, wp, wl, iteration):
            
    y_new = np.ndarray(shape=(50,50), dtype=float)
    for i in range(50):
        for j in range(50):
            y_new[i, j] = 0
            
    for i in range(50):
        for j in range(50):
            num = random.uniform(0, 1)
            #print(num)
    
            p = computeConditionalDistribution(y, i, j, x, wp, wl)
    
            #print("ghabl= " + str(y[i, j]))
            if(num < p):
                 y[i, j] = 1
            else:
                 y[i, j] = 0
    
            #print("bad= " + str(y[i, j]))
    return y


# In[35]:

def computePosteriorMean(t):
    iter_num = t
    
    posterior_image = []
    for i in range(50):
        for j in range(50):
            summation = 0
            for s in range(iter_num):
                cur_y = all_samples[s]
                summation = summation + cur_y[i, j]
            posterior_image.append(summation)
    
    for i in range(2500):
        posterior_image[i] = (posterior_image[i] / iter_num)
        
    #cur_mae = computeMAE(posterior_image)
    #maes.append(cur_mae)
    
    #print("error= " + str(cur_mae) + "*" + str(t))
    
    #if(t % 1000 == 0):
    #f = np.ndarray(shape = (50, 50), buffer = np.array(posterior_image))
        
    #plt.imshow(f, cmap='gray')
    #plt.savefig('/Users/anahita/Desktop/plot' + str(t) + '.pdf')


# In[36]:

import math
def computeMAE(posterior_mean):
    #read stripes.txt
    file = open('/Users/anahita/Desktop/stripes.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(2500):
        x[i] = float(x[i])
            
    h = 50
    w = 50
    error = 0
    for i in range(50):
        for j in range(50):
            error = error + math.fabs(x[50 * i + j] - posterior_mean[50 * i + j])
    return (error / (h * w))


# In[37]:

import math
def computeMAEBaseLine():
    #read stripes.txt
    file = open('/Users/anahita/Desktop/stripes.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(50):
        for j in range(50):
            x[50 * i + j] = float(x[50 * i + j])
            
    #read stripes_noise.txt
    file = open('/Users/anahita/Desktop/stripes-noise.txt', 'r')
    ys = file.read()
    y = ys.split()
    for i in range(50):
        for j in range(50):
            y[50 * i + j] = float(y[50 * i + j])
            
    h = 50
    w = 50
    error = 0
    for i in range(2500):
        error = error + math.fabs(x[i] - y[i])
        
    return (error / (h * w))


# In[40]:

import numpy as np
import matplotlib.pyplot as pltme
#draws the plot for question 2.d
def draw2D(pixel_on1, pixel_on2):
    ##first list
    x_axis = []
    me = []
    for i in range(1000):
        x_axis.append(i + 1)
        
    pltme.plot(x_axis, pixel_on1)
    pltme.plot(x_axis, pixel_on2)
    pltme.xlabel("iteration")
    pltme.ylabel("pixel_on")
    
    pltme.savefig('/Users/anahita/Desktop/2dNew22.pdf')
    pltme.show()


# In[ ]:

import numpy as np
#import matplotlib.pyplot as plt
def gibbsSampler(wp, wl, num):
    #read the image
    file = open('/Users/anahita/Desktop/stripes-noise.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(2500):
        x[i] = float(x[i])
    
    #initialization phase
    xx= np.ndarray(shape = (50, 50), buffer = np.array(x))
    y = np.ndarray(shape = (50, 50), buffer = np.array(x))
            
    #initialization to zero
    if(num == 2):
        for i in range(50):
            for j in range(50):
                y[i, j] = 0
        
                
    #plt.imshow(y, cmap='gray')
    #plt.savefig('/Users/anahita/Desktop/plot.pdf')
    
    #print("baseline error= " + str(computeMAEBaseLine()))
            
    iter_num = 1000
    #iterations
    for t in range(iter_num):
        #sweep
        #print(y)
        y_new = sweep(y, xx, wp, wl, t)
        #complete a sample
        #f = np.ndarray(shape = (50, 50), buffer = np.array(y_new))
        
        #plt.imshow(f, cmap='gray')
        #plt.savefig('/Users/anahita/Desktop/plot' + str(t) + '.pdf')
        
        for i in range(50):
            for j in range(50):
                y[i, j] = y_new[i, j]
                
        all_samples.append(y_new)
        computePosteriorMean(t + 1)
        pix_on = 0
        for i in range(50):
            for j in range(50):
                if(y[i, j] == 1):
                    pix_on = pix_on + 1
        if(num == 1):
            pixel_on1.append(pix_on/2500)
            print("pixel_on1= " + str(pix_on/2500))
        else:
            pixel_on2.append(pix_on/2500)
            print("pixel_on2= " + str(pix_on/2500))
        
all_samples = []
pixel_on1 = []
gibbsSampler(2, 2, 1)
pixel_on2 = []
gibbsSampler(2, 2, 2)
print("done")
draw2D(pixel_on1, pixel_on2)


# In[ ]:




# In[13]:




# In[110]:

1 - 0.00000000e+00


# In[ ]:



