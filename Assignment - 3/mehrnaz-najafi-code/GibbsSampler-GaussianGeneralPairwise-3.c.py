
# coding: utf-8

# In[36]:

import math
#case1: i = 0  and j = 0
#two neighbours: (0, 1) and (1, 0)
def computeRegion1(y , i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[0, 1]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[1, 0]) ** 2)
    
    variance = wp1 + wp2 + wl
    variance = 1/(2 * variance)
    
    
    mu = (2 * variance) * (wp1 * y[0, 1] + wp2 * y[1, 0] + wl * x[0, 0])

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[37]:

import math
#case2: i = 49 and j = 0
#two neighbours: (48, 0) and (49, 1)
def computeRegion2(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[48, 0]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[49, 1]) ** 2)
    
    variance = wp1 + wp2 + wl
    variance = 1/(2 * variance)
    
    mu = (2 * variance) * (wp1 * y[48, 0] + wp2 * y[49, 1] + wl * x[i, j])

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[38]:

import math
#case3: i = 0 and j = 49
#two neighbours: (0, 48) and (1, 49)
def computeRegion3(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[0, 48]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[1, 49]) ** 2)
    
    variance = wp1 + wp2 + wl
    variance = 1/(2 * variance)
    
    mu = (2 * variance) * (wp1 * y[0, 48] + wp2 * y[1, 49] + wl * x[i, j])

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[39]:

import math
#case4: i = 49 and j = 49
#two neighbours: (48, 49) and (49, 48)
def computeRegion4(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[48, 49]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[49, 48]) ** 2)
    
    variance = wp1 + wp2 + wl
    variance = 1/(2 * variance)
    
    mu = (2 * variance) * (wp1 * y[48, 49] + wp2 * y[49, 48] + wl * x[i, j])

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[40]:

import math
#case5: j = 0
def computeRegion5(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[i - 1, j]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[i + 1, j]) ** 2)
    wp3 = wp/(0.01 + (x[i, j] - x[i, j + 1]) ** 2)
    
    variance = wp1 + wp2 + wp3 + wl
    variance = 1/(2 * variance)
    
    mu = (wp1 * y[i - 1, j])
    mu = mu + (wp2 * y[i + 1, j])
    mu = mu + (wp3 * y[i, j + 1] + wl * x[i, j])
    mu = (2 * variance) * mu

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[41]:

import math
#case6: j = 49
def computeRegion6(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[i - 1, j]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[i + 1, j]) ** 2)
    wp3 = wp/(0.01 + (x[i, j] - x[i, j - 1]) ** 2)
    
    variance = wp1 + wp2 + wp3 + wl
    variance = 1/(2 * variance)
    
    mu = (wp1 * y[i - 1, j])
    mu = mu + (wp2 * y[i + 1, j])
    mu = mu + (wp3 * y[i, j - 1] + wl * x[i, j])
    mu = (2 * variance) * mu

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[42]:

import math
#case7: i = 0
def computeRegion7(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[i, j - 1]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[i, j + 1]) ** 2)
    wp3 = wp/(0.01 + (x[i, j] - x[i + 1, j]) ** 2)
    
    variance = wp1 + wp2 + wp3 + wl
    variance = 1/(2 * variance)
    
    mu = (wp1 * y[i, j - 1])
    mu = mu + (wp2 * y[i, j + 1])
    mu = mu + (wp3 * y[i + 1, j] + wl * x[i, j])
    mu = (2 * variance) * mu

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[43]:

import math
#case8: i = 49
def computeRegion8(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[i, j - 1]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[i, j + 1]) ** 2)
    wp3 = wp/(0.01 + (x[i, j] - x[i - 1, j]) ** 2)
    
    variance = wp1 + wp2 + wp3 + wl
    variance = 1/(2 * variance)
    
    mu = (wp1 * y[i, j - 1])
    mu = mu + (wp2 * y[i, j + 1])
    mu = mu + (wp3 * y[i - 1, j] + wl * x[i, j])
    mu = (2 * variance) * mu

    if(is_variance == 1):
        return variance
    else:
        return mu


# In[44]:

import math
#case9: otherwise
def computeRegion9(y, i, j, x, wp, wl, is_variance):
    wp1 = wp/(0.01 + (x[i, j] - x[i, j - 1]) ** 2)
    wp2 = wp/(0.01 + (x[i, j] - x[i, j + 1]) ** 2)
    wp3 = wp/(0.01 + (x[i, j] - x[i + 1, j]) ** 2)
    wp4 = wp/(0.01 + (x[i, j] - x[i - 1, j]) ** 2)
    
    variance = wp1 + wp2 + wp3 + wp4 + wl
    variance = 1/(2 * variance)
    
    mu = (wp1 * y[i, j - 1])
    mu = mu + (wp2 * y[i, j + 1])
    mu = mu + (wp3 * y[(i + 1), j])
    mu = mu + (wp4 * y[(i - 1), j] + wl * x[i, j])
    mu =(2 * variance) * mu
    
    if(is_variance == 1):
        return variance
    else:
        return mu


# In[45]:

import math
import numpy as np
def computeVarianceMu(y, i, j, x, wp, wl, is_variance):
    if ((j == 0) or (j == 49)):
        if((i == 0) and (j == 0)):
            return computeRegion1(y, i, j, x, wp, wl, is_variance)
        if((i == 49) and (j == 0)):
            return computeRegion2(y, i, j, x, wp, wl, is_variance)
        if((i == 0) and (j == 49)):
            return computeRegion3(y, i, j, x, wp, wl, is_variance)
        if((i == 49) and (j == 49)):
            return computeRegion4(y, i, j, x, wp, wl, is_variance)
        if(j == 0):
            return computeRegion5(y, i, j, x, wp, wl, is_variance)
        if(j == 49):
            return computeRegion6(y, i, j, x, wp, wl, is_variance)
    if(i == 0):
        return computeRegion7(y, i, j, x, wp, wl, is_variance)
    if(i == 49):
        return computeRegion8(y, i, j, x, wp, wl, is_variance)
    return computeRegion9(y, i, j, x, wp, wl, is_variance)


# In[ ]:




# In[46]:

import random
import matplotlib.pyplot as plt
import math
import numpy as np
def sweep(y, x, wp, wl, iteration):
            
    y_new = np.ndarray(shape=(50,50), dtype=float)
    for i in range(50):
        for j in range(50):
            y_new[i, j] = y[i, j]
            
    for i in range(50):
        for j in range(50):
            num = random.gauss(0, 1)
            #print(num)
            #rand
    
            variance = computeVarianceMu(y_new, i, j, x, wp, wl, 1)
            #print(type(variance) + str(i) + str(j))
        
            mu = computeVarianceMu(y_new, i, j, x, wp, wl, 2)
            #print(mu)
            
            y_new[i, j] = mu + num * math.sqrt(variance)
    
    return y_new


# In[57]:

def computePosteriorMean(t):
    iter_num = t
    
    posterior_image = []
    for i in range(50):
        for j in range(50):
            summation = 0
            for s in range(iter_num):
                cur_y = all_samples[s]
                summation = summation + cur_y[i, j]
            posterior_image.append(summation)
    
    for i in range(2500):
        posterior_image[i] = (posterior_image[i] / iter_num)
        
    cur_mae = computeMAE(posterior_image)
    maes.append(cur_mae)
    
    print("error= " + str(cur_mae) + "*" + str(t))
    
    if(t == 100):
        f = np.ndarray(shape = (50, 50), buffer = np.array(posterior_image))
        
        plt.imshow(f, cmap='gray')
        plt.savefig('/Users/anahita/Desktop/plot3cNewNight' + str(t) + '.pdf')


# In[58]:

import math
def computeMAE(posterior_mean):
    #read stripes.txt
    file = open('/Users/anahita/Desktop/swirl.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(2500):
        x[i] = float(x[i])
            
    h = 50
    w = 50
    error = 0
    for i in range(50):
        for j in range(50):
            error = error + math.fabs(x[50 * i + j] - posterior_mean[50 * i + j])
    return (error / (h * w))


# In[59]:

import math
def computeMAEBaseLine():
    #read stripes.txt
    file = open('/Users/anahita/Desktop/swirl.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(50):
        for j in range(50):
            x[50 * i + j] = float(x[50 * i + j])
            
    #read stripes_noise.txt
    file = open('/Users/anahita/Desktop/swirl-noise.txt', 'r')
    ys = file.read()
    y = ys.split()
    for i in range(50):
        for j in range(50):
            y[50 * i + j] = float(y[50 * i + j])
            
    h = 50
    w = 50
    error = 0
    for i in range(2500):
        error = error + math.fabs(x[i] - y[i])
        
    return (error / (h * w))

print(computeMAEBaseLine())


# In[ ]:




# In[61]:

import numpy as np
import matplotlib.pyplot as plt
def gibbsSampler(wp, wl):
    #read the image
    file = open('/Users/anahita/Desktop/swirl-noise.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(2500):
        x[i] = float(x[i])
    
    #initialization phase
    xx= np.ndarray(shape = (50, 50), buffer = np.array(x))
    y = np.ndarray(shape = (50, 50), buffer = np.array(x))
            
    plt.imshow(y, cmap='gray')
    plt.savefig('/Users/anahita/Desktop/plotMe.pdf')
    
    #print("baseline error= " + str(computeMAEBaseLine()))
            
    iter_num = 100
    #iterations
    for t in range(iter_num):
        #sweep
        y_new = sweep(y, xx, wp, wl, t)
        #complete a sample
        #f = np.ndarray(shape = (50, 50), buffer = np.array(y_new))
        
        #plt.imshow(f, cmap='gray')
        #plt.savefig('/Users/anahita/Desktop/plot' + str(t) + '.pdf')
        
        for i in range(50):
            for j in range(50):
                y[i, j] = y_new[i, j]
                
        all_samples.append(y_new)
        computePosteriorMean(t + 1)
        
all_samples = []
maes = []
gibbsSampler(5, 60)


# In[ ]:




# In[13]:




# In[ ]:




# In[ ]:



