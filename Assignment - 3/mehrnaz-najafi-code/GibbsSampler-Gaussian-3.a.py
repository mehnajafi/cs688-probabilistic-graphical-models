
# coding: utf-8

# In[129]:

import math
#case1: i = 0  and j = 0
#two neighbours: (0, 1) and (1, 0)
def computeRegion1(y , i, j, x, wp, wl):
    first_term = wp * y[0, 1] + wp * y[1, 0] + wl * x[0, 0]

    return (first_term / (wp * 2 + wl))


# In[130]:

import math
#case2: i = 49 and j = 0
#two neighbours: (48, 0) and (49, 1)
def computeRegion2(y, i, j, x, wp, wl):
    first_term = wp * y[48, 0] + wp * y[49, 1] + wl * x[i, j]

    return (first_term / (wp * 2 + wl))


# In[131]:

import math
#case3: i = 0 and j = 49
#two neighbours: (0, 48) and (1, 49)
def computeRegion3(y, i, j, x, wp, wl):
    first_term = wp * y[0, 48] + wp * y[1, 49] + wl * x[i, j]

    return (first_term / (wp * 2 + wl))


# In[132]:

import math
#case4: i = 49 and j = 49
#two neighbours: (48, 49) and (49, 48)
def computeRegion4(y, i, j, x, wp, wl):
    first_term = wp * y[48, 49] + wp * y[49, 48] + wl * x[i, j]

    return (first_term / (wp * 2 + wl))


# In[133]:

import math
#case5: j = 0
def computeRegion5(y, i, j, x, wp, wl):
    first_term = (wp * y[i - 1, j])
    first_term = first_term + (wp * y[i + 1, j])
    first_term = first_term + (wp * y[i, j + 1] + wl * x[i, j])

    return (first_term / (wp * 3 + wl))


# In[134]:

import math
#case6: j = 49
def computeRegion6(y, i, j, x, wp, wl):
    first_term = (wp * y[i - 1, j])
    first_term = first_term + (wp * y[i + 1, j])
    first_term = first_term + (wp * y[i, j - 1] + wl * x[i, j])

    return (first_term / (wp * 3 + wl))


# In[135]:

import math
#case7: i = 0
def computeRegion7(y, i, j, x, wp, wl):
    first_term = (wp * y[i, j - 1])
    first_term = first_term + (wp * y[i, j + 1])
    first_term = first_term + (wp * y[i + 1, j] + wl * x[i, j])

    return (first_term / (wp * 3 + wl))


# In[136]:

import math
#case8: i = 49
def computeRegion8(y, i, j, x, wp, wl):
    first_term = (wp * y[i, j - 1])
    first_term = first_term + (wp * y[i, j + 1])
    first_term = first_term + (wp * y[i - 1, j] + wl * x[i, j])

    return (first_term / (wp * 3 + wl))


# In[137]:

import math
#case9: otherwise
def computeRegion9(y, i, j, x, wp, wl):
    first_term = (wp * y[i, j - 1])
    first_term = first_term + (wp * y[i, j + 1])
    first_term = first_term + (wp * y[(i + 1), j])
    first_term = first_term + (wp * y[(i - 1), j] + wl * x[i, j])

    return (first_term / (wp * 4 + wl))


# In[138]:

import math
import numpy as np
def computeMu(y, i, j, x, wp, wl):
    if ((j == 0) or (j == 49)):
        if((i == 0) and (j == 0)):
            return computeRegion1(y, i, j, x, wp, wl)
        if((i == 49) and (j == 0)):
            return computeRegion2(y, i, j, x, wp, wl)
        if((i == 0) and (j == 49)):
            return computeRegion3(y, i, j, x, wp, wl)
        if((i == 49) and (j == 49)):
            return computeRegion4(y, i, j, x, wp, wl)
        if(j == 0):
            return computeRegion5(y, i, j, x, wp, wl)
        if(j == 49):
            return computeRegion6(y, i, j, x, wp, wl)
    if(i == 0):
        return computeRegion7(y, i, j, x, wp, wl)
    if(i == 49):
        return computeRegion8(y, i, j, x, wp, wl)
    return computeRegion9(y, i, j, x, wp, wl)


# In[139]:

def computeVariance(i, j, wp, wl):
    if ((j == 0) or (j == 49)):
        if((i == 0) and (j == 0)):
            return (1/(2 *(wp * 2 + wl)))
        if((i == 49) and (j == 0)):
            return (1/(2 *(wp * 2 + wl)))
        if((i == 0) and (j == 49)):
            return (1/(2 *(wp * 2 + wl)))
        if((i == 49) and (j == 49)):
            return (1/(2 *(wp * 2 + wl)))
        if(j == 0):
            return (1/(2 *(wp * 3 + wl)))
        if(j == 49):
            return (1/(2 *(wp * 3 + wl)))
    if(i == 0):
        return (1/(2 *(wp * 3 + wl)))
    if(i == 49):
        return (1/(2 *(wp * 3 + wl)))
    return (1/(2 *(wp * 4 + wl)))


# In[140]:

import random
import matplotlib.pyplot as plt
import math
import numpy as np
def sweep(y, x, wp, wl, iteration):
            
    y_new = np.ndarray(shape=(50,50), dtype=float)
    for i in range(50):
        for j in range(50):
            y_new[i, j] = y[i, j]
            
    for i in range(50):
        for j in range(50):
            num = random.gauss(0, 1)
            #print(num)
            #rand
    
            variance = computeVariance(i, j, wp, wl)
            #print(type(variance) + str(i) + str(j))
        
            mu = computeMu(y_new, i, j, x, wp, wl)
            #print(mu)
            
            y_new[i, j] = mu + num * math.sqrt(variance)
    
    return y_new


# In[141]:

def computePosteriorMean(t):
    iter_num = t
    
    posterior_image = []
    for i in range(50):
        for j in range(50):
            summation = 0
            for s in range(iter_num):
                cur_y = all_samples[s]
                summation = summation + cur_y[i, j]
            posterior_image.append(summation)
    
    for i in range(2500):
        posterior_image[i] = (posterior_image[i] / iter_num)
        
    cur_mae = computeMAE(posterior_image)
    maes.append(cur_mae)
    
    print("error= " + str(cur_mae) + "*" + str(t))
    
    if(t == 100):
        f = np.ndarray(shape = (50, 50), buffer = np.array(posterior_image))
        
        plt.imshow(f, cmap='gray')
        plt.savefig('/Users/anahita/Desktop/plot3aNewNew' + str(t) + '.pdf')


# In[142]:

import math
def computeMAE(posterior_mean):
    #read stripes.txt
    file = open('/Users/anahita/Desktop/swirl.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(2500):
        x[i] = float(x[i])
            
    h = 50
    w = 50
    error = 0
    for i in range(50):
        for j in range(50):
            error = error + math.fabs(x[50 * i + j] - posterior_mean[50 * i + j])
    return (error / (h * w))


# In[143]:

import math
def computeMAEBaseLine():
    #read stripes.txt
    file = open('/Users/anahita/Desktop/swirl.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(50):
        for j in range(50):
            x[50 * i + j] = float(x[50 * i + j])
            
    #read stripes_noise.txt
    file = open('/Users/anahita/Desktop/swirl-noise.txt', 'r')
    ys = file.read()
    y = ys.split()
    for i in range(50):
        for j in range(50):
            y[50 * i + j] = float(y[50 * i + j])
            
    h = 50
    w = 50
    error = 0
    for i in range(2500):
        error = error + math.fabs(x[i] - y[i])
        
    return (error / (h * w))

print(computeMAEBaseLine())


# In[ ]:




# In[145]:

import numpy as np
import matplotlib.pyplot as plt
def gibbsSampler(wp, wl):
    #read the image
    file = open('/Users/anahita/Desktop/swirl-noise.txt', 'r')
    xs = file.read()
    x = xs.split()
    for i in range(2500):
        x[i] = float(x[i])
    
    #initialization phase
    xx= np.ndarray(shape = (50, 50), buffer = np.array(x))
    y = np.ndarray(shape = (50, 50), buffer = np.array(x))
            
    plt.imshow(y, cmap='gray')
    plt.savefig('/Users/anahita/Desktop/plotMe.pdf')
    
    #print("baseline error= " + str(computeMAEBaseLine()))
            
    iter_num = 100
    #iterations
    for t in range(iter_num):
        #sweep
        y_new = sweep(y, xx, wp, wl, t)
        #complete a sample
        #f = np.ndarray(shape = (50, 50), buffer = np.array(y_new))
        
        #plt.imshow(f, cmap='gray')
        #plt.savefig('/Users/anahita/Desktop/plot' + str(t) + '.pdf')
        
        for i in range(50):
            for j in range(50):
                y[i, j] = y_new[i, j]
                
        all_samples.append(y_new)
        computePosteriorMean(t + 1)
        
#10, 200
all_samples = []
maes = []
gibbsSampler(10, 100)


# In[ ]:




# In[13]:




# In[110]:

1 - 0.00000000e+00


# In[ ]:



