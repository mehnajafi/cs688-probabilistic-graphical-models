
# coding: utf-8

# In[26]:

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
#generates the image for 3.1
def main1():
    labels = ['E','T','A','I','N','O','S','H','R','D']
    ##wFeatures = np.random.rand(10,321) #Replace with learned feature parameters
    yy = open('/Users/anahita/Desktop/params.txt')

    param_list = []
    for p in range(10 * 321 + 10 * 10):
        param = yy.readline()
        param = param[0:len(param) - 1]
        param_list.append(float(param))
    
    temp = np.array(param_list[10*321:])
    wTrans    = temp.reshape(10, 10)
    #print(wTrans[0, 4])
    #print(wTrans[9, 0])
    #print(wTrans[5, 4])
    #np.random.rand(10,10)  #Replace with learned transition parameters
  
    #plt.figure(1)
    #for i in range(10):
    #plt.subplot(2,5,i+1);
    #plt.imshow(np.reshape(wFeatures[i,1:],(16,20).'T'),interpolation='nearest',cmap = cm.Greys_r);
    #plt.title(labels[i]);
    #plt.colorbar(shrink=0.6)
    #plt.xticks([])
    #plt.yticks([])
    
    plt.figure(2)
    plt.imshow(wTrans,interpolation='nearest',cmap = cm.Greys_r);
    plt.xticks(range(10),labels)
    plt.yticks(range(10),labels)
    plt.colorbar();
  
    plt.show(block = False)
    plt.plot()
    plt.savefig('/Users/anahita/Desktop/plot.pdf')
main1()


# In[27]:

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

#generates the image for 3.2
def main2():
    #labels = ['E','T','A','I','N','O','S','H','R','D']
    ##wFeatures = np.random.rand(10,321) #Replace with learned feature parameters
    yy = open('/Users/anahita/Desktop/params.txt')

    param_list = []
    for p in range(10 * 321 + 10 * 10):
        param = yy.readline()
        param = param[0:len(param) - 1]
        param_list.append(float(param))
    
    
    new_labels = ['E', 'A', 'I', 'O', 'T', 'N', 'S', 'H', 'R', 'D']
    new_temp = param_list[10*321:]
    new_param_list = []
    
    #E
    new_param_list.append(new_temp[0])
    new_param_list.append(new_temp[2])
    new_param_list.append(new_temp[3])
    new_param_list.append(new_temp[5])
    new_param_list.append(new_temp[1])
    new_param_list.append(new_temp[4])
    new_param_list.append(new_temp[6])
    new_param_list.append(new_temp[7])
    new_param_list.append(new_temp[8])
    new_param_list.append(new_temp[9])
    
    #A
    new_param_list.append(new_temp[20])
    new_param_list.append(new_temp[22])
    new_param_list.append(new_temp[23])
    new_param_list.append(new_temp[25])
    new_param_list.append(new_temp[21])
    new_param_list.append(new_temp[24])
    new_param_list.append(new_temp[26])
    new_param_list.append(new_temp[27])
    new_param_list.append(new_temp[28])
    new_param_list.append(new_temp[29])
    
    #I
    new_param_list.append(new_temp[30])
    new_param_list.append(new_temp[32])
    new_param_list.append(new_temp[33])
    new_param_list.append(new_temp[35])
    new_param_list.append(new_temp[31])
    new_param_list.append(new_temp[34])
    new_param_list.append(new_temp[36])
    new_param_list.append(new_temp[37])
    new_param_list.append(new_temp[38])
    new_param_list.append(new_temp[39])
    
    #O
    new_param_list.append(new_temp[50])
    new_param_list.append(new_temp[52])
    new_param_list.append(new_temp[53])
    new_param_list.append(new_temp[55])
    new_param_list.append(new_temp[51])
    new_param_list.append(new_temp[54])
    new_param_list.append(new_temp[56])
    new_param_list.append(new_temp[57])
    new_param_list.append(new_temp[58])
    new_param_list.append(new_temp[59])
    
    #T
    new_param_list.append(new_temp[10])
    new_param_list.append(new_temp[12])
    new_param_list.append(new_temp[13])
    new_param_list.append(new_temp[15])
    new_param_list.append(new_temp[11])
    new_param_list.append(new_temp[14])
    new_param_list.append(new_temp[16])
    new_param_list.append(new_temp[17])
    new_param_list.append(new_temp[18])
    new_param_list.append(new_temp[19])
    
    #N
    new_param_list.append(new_temp[40])
    new_param_list.append(new_temp[42])
    new_param_list.append(new_temp[43])
    new_param_list.append(new_temp[45])
    new_param_list.append(new_temp[41])
    new_param_list.append(new_temp[44])
    new_param_list.append(new_temp[46])
    new_param_list.append(new_temp[47])
    new_param_list.append(new_temp[48])
    new_param_list.append(new_temp[49])
    
    #S
    new_param_list.append(new_temp[60])
    new_param_list.append(new_temp[62])
    new_param_list.append(new_temp[63])
    new_param_list.append(new_temp[65])
    new_param_list.append(new_temp[61])
    new_param_list.append(new_temp[64])
    new_param_list.append(new_temp[66])
    new_param_list.append(new_temp[67])
    new_param_list.append(new_temp[68])
    new_param_list.append(new_temp[69])
    
    #H
    new_param_list.append(new_temp[70])
    new_param_list.append(new_temp[72])
    new_param_list.append(new_temp[73])
    new_param_list.append(new_temp[75])
    new_param_list.append(new_temp[71])
    new_param_list.append(new_temp[74])
    new_param_list.append(new_temp[76])
    new_param_list.append(new_temp[77])
    new_param_list.append(new_temp[78])
    new_param_list.append(new_temp[79])
    
    #R
    new_param_list.append(new_temp[80])
    new_param_list.append(new_temp[82])
    new_param_list.append(new_temp[83])
    new_param_list.append(new_temp[85])
    new_param_list.append(new_temp[81])
    new_param_list.append(new_temp[84])
    new_param_list.append(new_temp[86])
    new_param_list.append(new_temp[87])
    new_param_list.append(new_temp[88])
    new_param_list.append(new_temp[89])
    
    #D
    new_param_list.append(new_temp[90])
    new_param_list.append(new_temp[92])
    new_param_list.append(new_temp[93])
    new_param_list.append(new_temp[95])
    new_param_list.append(new_temp[91])
    new_param_list.append(new_temp[94])
    new_param_list.append(new_temp[96])
    new_param_list.append(new_temp[97])
    new_param_list.append(new_temp[98])
    new_param_list.append(new_temp[99])
    
    temp = np.array(new_param_list)
    wTrans    = temp.reshape(10, 10)
    
    print(wTrans[3, 8])
    print(wTrans[9, 0])
    print(wTrans[0, 5])
    #np.random.rand(10,10)  #Replace with learned transition parameters
  
    #plt.figure(1)
    #for i in range(10):
    #plt.subplot(2,5,i+1);
    #plt.imshow(np.reshape(wFeatures[i,1:],(16,20).'T'),interpolation='nearest',cmap = cm.Greys_r);
    #plt.title(labels[i]);
    #plt.colorbar(shrink=0.6)
    #plt.xticks([])
    #plt.yticks([])
    
    plt.figure(3)
    plt.imshow(wTrans,interpolation='nearest',cmap = cm.Greys_r);
    plt.xticks(range(10),new_labels)
    plt.yticks(range(10),new_labels)
    plt.colorbar();
  
    plt.show(block = False)
    plt.plot()
    plt.savefig('/Users/anahita/Desktop/plotNew.pdf')
main2()


# In[ ]:




# In[ ]:



