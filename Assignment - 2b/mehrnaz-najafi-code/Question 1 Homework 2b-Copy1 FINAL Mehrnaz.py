
# coding: utf-8

# In[40]:

def getLabelPos (word, j):
    c = word[j]
    
    labels = "etainoshrd"
    for index in range(len(labels)):
        if(c == labels[index]):
            return index

    return -1

#computes node potential for the position j of the test image i
#Should be changed for computing accuracy from train image to test image
def computeNodePotentialByWord(j, i, params):
    #param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    #params = param.read()
    parameters = params[0:10*321]
    #print (parameters)

    ####CHANGED AFTER TRAINING
    feature     = open('/Users/anahita/Desktop/train_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)

    ##step_ijf = 0
    #positions = []
   
    #Change: Thursday
    ##step_ijf = 0
    ##for index in range(j):
    step_ijf = 321 * j
    
    ##for j in range(len(word)):
    position_label = []
    for yj in range(10):
        cf  = 0
        value = 0
        for c in range(10):
            x_ijf = step_ijf
            
            if(yj == c):
                for f in range(0, 321):
                # Wcf * xijf
                #Change: Thursday
                ##temp = float(parameters[cf]) * float(observations[x_ijf])
                #QUESTION: Am I right?
                #if(getLabelByPos(word, j) != c):
                #    temp = 0
                #if(yj == c):
                    #Change: Thursday
                    ##temp = 0
                    value = value + float(parameters[cf]) * float(observations[x_ijf])
                    cf = cf + 1
                    x_ijf = x_ijf + 1
            else:
                cf = cf + 321
                x_ijf = x_ijf + 321
        position_label.append(value)

    #step_ijf = step_ijf + 321
    #positions.append(position_label)
        
    #print (positions)
    return (position_label)
#position - 1


# In[25]:

#computes the transition potential 
def computeTransitionPotentialByWordMod(j, i, params):
    #param = open('/Users/anahita/Desktop/transition-params.txt', 'r')
    #params = param.read()
    parameters = params[10*321:]
    #print (parameters)
    
    #Change2: Thursday
    ##transition_position = []
    #for j in range(len(word) - 1): 
    ##for c1 in range (10):
        ##for c2 in range(10):
            ##w = float(parameters[c1 * 10 + c2])
            #if(c1 == 1 and c2 == 1):
            #    print(w)
            ##transition_position.append(w)
        
    ##return transition_position
    #transition_position = []
    #for j in range(len(word) - 1): 
    #for c1 in range (10):
    #    for c2 in range(10):
    #        w = float(params[c1 * 10 + c2])
    #        #if(c1 == 1 and c2 == 1):
            #    print(w)
    #     transition_position.append(w)
        
    #return transition_position
    return parameters

#print("salam")

#computeTransitionPotentialByWordMod(0, 1, param_list)


# In[26]:

#computes clique potential 
#note that it is being computed for the first test word only
def computeCliquePotential(j, word_length, word_number, params):
    node_potential = computeNodePotentialByWord(j, word_number, params)
    
    #Change: Thursday
    ###transition_potential = computeTransitionPotentialByWordMod(j, word_number, params)
    transition_potential = params[10*321:]
    
    for ij in range(10):
        for ijp in range(10):
            transition_potential[ij * 10 + ijp] = transition_potential[ij * 10 + ijp] + node_potential[ij]
            
    if(j == (word_length - 2)):
        another_node_potential = computeNodePotentialByWord(j + 1, word_number, params)
        for sj in range(10):
            for sjp in range(10):
                transition_potential[sj * 10 + sjp] = transition_potential[sj * 10 + sjp] + another_node_potential[sjp]
    
    return (transition_potential)

#yyr = open('/Users/anahita/Desktop/params.txt')

#param_list = []
#for pof in range(10 * 321 + 10 * 10):
#    paramr = yyr.readline()
#    paramr = paramr[0:len(paramr) - 1]
#    param_list.append(float(paramr))
#computeCliquePotential(2, 4, 1, param_list)


# In[27]:

import math
#implementation of the logsumexp algorithm presented in the lecture notes
def computeLogSumExp(vector):
    maximum = vector[0]
    for i in range(1, len(vector)):
        if(vector[i] > maximum):
            maximum = vector[i]
    
    for i in range(len(vector)):
        vector[i] = vector[i] - maximum
    
    summation = 0
    for i in range(len(vector)):
        summation = summation + math.exp(vector[i])
    
    return (maximum + math.log(summation)) 

#computes backward message from i to j for the word whose length is word_length and is the word_number th word in the 
#test_words.txt 
def computeMessageBackward(i, j, word_number, word_length, params):
    
    clique_potential = computeCliquePotential(i - 1, word_length, word_number, params)
    
    result = []
    if(i == (word_length - 1)):
        #print("salam")
        for ym in range(10):
            summation = 0
            for yn in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn])
            result.append(math.log(summation))
    else:
        message = computeMessageBackward(i + 1, i, word_number, word_length, params)
        for ym in range(10):
            summation = 0
            for yn in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn] + message[yn])
            result.append(math.log(summation))
    return result

#print(computeMessageBackward(2, 1, 1))


# In[28]:

#computes forward messages (from i to j) for the word in word_number th row of the test_words.txt 
#and whose length is word_length
def computeMessageForward(i, j, word_number, word_length, params):
    #y           = open('/Users/anahita/Desktop/test_words.txt')
    #word = ''
    #for l in range(word_number):
    #    word = y.readline()
    #word_length = len(word) - 1
    
    clique_potential = computeCliquePotential(i - 1, word_length, word_number, params)
    
    result = []
    if(i == 1):
        #print("salam")
        for yn in range(10):
            summation = 0
            for ym in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn])
            result.append(math.log(summation))
    else:
        message = computeMessageForward(i - 1, i, word_number, word_length, params)
        for yn in range(10):
            summation = 0
            for ym in range(10):
                summation = summation + math.exp(clique_potential[ym * 10 + yn] + message[ym])
            result.append(math.log(summation))
    return result

#print(computeMessageForward(2, 3, 1))


# In[29]:

#computes belief i for the word in row = word_number of the test_words.txt (length of the word in word_length)
def computeBelief(i, word_length, word_number, params):
    #y           = open('/Users/anahita/Desktop/train_words.txt')
    #word = ''
    #for l in range(word_number):
    #    word = y.readline()
    #word_length = len(word) - 1
    
    clique_potential = computeCliquePotential(i - 1, word_length, word_number, params)
    
    message = []
    message_m = []
    belief = []
    if(i == 1):
        message = computeMessageBackward(i + 1, i, word_number, word_length, params)
        for ym in range(10):
            for yn in range(10):
                belief.append(clique_potential[ym * 10 + yn] + message[yn])
    if(i == (word_length - 1)):
        message = computeMessageForward(i - 1, i, word_number, word_length, params)
        for ym in range(10):
            for yn in range(10):
                belief.append(clique_potential[ym * 10 + yn] + message[yn])
    if((i != 1) and (i != (word_length - 1))):
        message = computeMessageForward(i - 1, i, word_number, word_length, params)
        message_m = computeMessageBackward(i + 1, i, word_number, word_length, params)
        for ym in range(10):
            for yn in range(10):
                belief.append(clique_potential[ym * 10 + yn] + message[ym] + message[yn])
    
    return belief
    
#print(parameters)
#computeBelief(1, 4, 1, parameters)
    
#print(computeBelief(1, 4, 1, param_list))


# In[30]:

#Change: Thursday
import numpy as np
#computes single marginal given word length, the position, word number and the parameters
def computeSingleMarginal(l, j, i, params):
    word_length = l
    word_number = i
    
    #print("i= " + str(i))
    #print("j= " + str(j))
    
    bel = []
    if(j != (word_length - 1)):
        bel = computeBelief(j + 1, word_length, word_number, params)
    else:
        bel = computeBelief(j, word_length, word_number, params)
    
    #if(i == 1 and j == 0 and l == 8):
    #    print(bel)
    s = 0
    for y1 in range(10):
        for y2 in range(10):
            s = s + math.exp(bel[y1 * 10 + y2])
            #print(s)
         
    #Change: Wednesday    
    #joint_prob = []
    #for ym in range(10):
    #    for yn in range(10):
    #        joint_prob.append(math.exp(belief[ym * 10 + yn]) / second_part)
           
    result = []
    if(j != (word_length - 1)):
        for ym in range(10):
            summation = 0
            for yn in range(10):
                #summation = summation + joint_prob[ym * 10 + yn]
                summation = summation + (math.exp(bel[ym * 10 + yn]) / s)
            result.append(summation)
    else:
        for yn in range(10):
            summation = 0
            for ym in range(10):
                #summation = summation + joint_prob[ym * 10 + yn]
                summation = summation + (math.exp(bel[ym * 10 + yn]) / s)
            result.append(summation)
            
    ##return result[c]
    ##Change: Thursday
    final_result = np.asarray(result)
    return final_result.reshape(-9, 1)

#print(p)
#yyr = open('/Users/anahita/Desktop/params.txt')

#param_list = []
#for pof in range(10 * 321 + 10 * 10):
#    paramr = yyr.readline()
#    paramr = paramr[0:len(paramr) - 1]
#    param_list.append(float(paramr))
#a = computeSingleMarginal(4, 0, 1, param_list)
#print(a)


# In[31]:

#computes pairwise marginal given word length, position, word number and the parameters
def computePairwiseMarginal(l, j, i, params):
    word_length = l
    word_number = i
    
    #print("i =" + str(i))
    #print("j =" + str(j))
        
    belief = []
    if(j != (word_length - 1)):
        belief = computeBelief(j + 1, word_length, word_number, params)
    else:
        belief = computeBelief(j, word_length, word_number, params)
    
    second_part = 0
    for ym in range(10):
        for yn in range(10):
            second_part = second_part + math.exp(belief[ym * 10 + yn])
            
    joint_prob = []
    for ym in range(10):
        for yn in range(10):
            joint_prob.append(math.exp(belief[ym * 10 + yn]) / second_part)
            
    #return joint_prob[10 * c + cc]
    #Change: Thursday
    #return joint_prob
    final_result = np.asarray(joint_prob)
    #if(i == 1 and j == 0):
    #    print(m)
    return final_result.reshape(-90, 10)


#yy = open('/Users/anahita/Desktop/feature-params.txt')


#print(a)


# In[32]:

##############QUESTION 1 HOWEWORK 2B
#def getLabelPos2B(word, j):
#    c = word[j]
    
#    labels = "etainoshrd"
#    for index in range(len(labels)):
#        if(c == labels[index]):
#            return index
#    return -1
#computes node potential which will be used to compute energy value
#i is the word number, word is the word that we want to compute energy value for
def computeNodePotentialByWord2B(i, word, params):
    #param = open('/Users/anahita/Desktop/feature-params.txt', 'r')
    #params = param.read()
    #parameters = params.split()
    #print (parameters)
    parameters = params

    feature     = open('/Users/anahita/Desktop/train_img' + str(i) + '.txt')
    observs = feature.read()
    observations = observs.split()
    #print (observations)

    ##step_ijf = 0
    positions = []
    step_ijf = 0
    for j in range(len(word)):
        position_label = []
        cf  = 0
        value = 0
        for c in range(10):
            x_ijf = step_ijf
            
            if(getLabelPos(word, j) == c):
                for f in range(0, 321):
                    # Wcf * xijf
                    temp = float(parameters[cf]) * float(observations[x_ijf])
                    #QUESTION: Am I right?
                    #if(getLabelByPos(word, j) != c):
                    #    temp = 0
                    #Change: Thursday
                    ##if(getLabelPos(word,j) != c):
                    ##    temp = 0
                    value = value + temp
                    cf = cf + 1
                    x_ijf = x_ijf + 1
            else:
                cf = cf + 321
                
        position_label.append(value)

        step_ijf = step_ijf + 321
        positions.append(position_label)
        
    #print (positions)
    return (positions)

##computeNodePotentialByWord(1, "sala")


# In[33]:

#computes the transition potential for the given word and parameters, which will be used to compute energy value later
def computeTransitionPotentialByWord2B(i, word, params):
    #param = open('/Users/anahita/Desktop/transition-params.txt', 'r')
    #params = param.read()
    #parameters = params.split()
    #print (parameters)
    parameters = params
    
    transition_position = []
    for j in range(len(word) - 1): 
        w = float(parameters[getLabelPos(word, j) * 10 + getLabelPos(word, j + 1)])
        transition_position.append(w)
        
    return transition_position


# In[34]:

#computes negative energy for the "i"th test word given the "word"
def computeEnergyForLabelSeq2B(i, word, params):
    #we want to compute the value for the first three test words
    #Change: Thursday
    ##file = open('/Users/anahita/Desktop/train_img' + str(i) + '.txt')
        
    #first summation term
    node_positions = computeNodePotentialByWord2B(i, word, params[0:10*321])
    term1 = node_positions[0][0]
    #compute \sum \limits_{j = 1}^{L_{i}}
    for j in range(1, len(word)):
        term1 = term1 + node_positions[j][0]
    #print("term1")
    #print(term1)
    
    
    #second summation term
    transition_positions = computeTransitionPotentialByWord2B(i, word, params[10*321:])
    term2 = float(transition_positions[0])
    #compute \sum \limits_{j = 1}^{L_{i} - 1}
    for j in range(1, len(word) - 1): 
        term2 = term2 + float(transition_positions[j])
    #print ("term2")
    #print (term2)
    
    return (term1 + term2)


# In[ ]:




# In[ ]:




# In[35]:

import math
#params[0]: 10 * 321
#params[1]: 10 * 10
#computes average log likelihood given the parameters
def computeAverageLogLikelihood(params):
    #Change: Saturday
    ###y           = open('/Users/anahita/Desktop/train_words.txt')
    n = 400
    
    p = params.tolist()
    ##likelihoods = []
    likelihoods_summation = 0
    for i in range(n):
        #Change: Saturday
        ###word = y.readline()
        word = words[i]
        # \n; so, len - 1 (we want to know the length of the true label)
        word = word[0:len(word)-1]
        l = len(word)
        #Change: Thursday
        #print(str(l))
        #print("i=" + str(i))
        belief = computeBelief(1, l, i + 1, p)
        #print(belief)
        ##summation = 0
        ##for b in range(l):
        ##    belief = beliefs[b]
        second_part = 0
        for ym in range(10):
            for yn in range(10):
                second_part = second_part + math.exp(belief[ym * 10 + yn])
        
        #conditional_prob = computeConditionalLikelihood(i + 1, word, second_part, params)
    
        ##loglikelihood = math.log(conditional_prob)
        
        ##likelihoods.append(math.log(conditional_prob))
        likelihoods_summation = likelihoods_summation + (-computeEnergyForLabelSeq2B(i + 1, word, p) + math.log(second_part))
        #print(likelihoods_summation)
        
    ##summation = 0    
    ##for index in range(len(likelihoods)):
    ##    summation = summation + likelihoods[index]
    likelihoods_summation = likelihoods_summation / n
    print("likelihood= " + str(likelihoods_summation))
    
    if(likelihoods_summation < -18):
        for s in range(len(p)):
            print(p[s])
    
    return (likelihoods_summation)

#p = []
#for i in range(10 * 321):
#    p.append(float(fff[i]))
    
#for j in range(10 * 10):
#    p.append(float(ttt[j]))
    
#print(computeAverageLogLikelihood(p))
#print(computeBelief(1, 8, 1, p))


# In[ ]:




# In[36]:

import math
import numpy as np
#computes the derivative of the average log likelihood with respect to wcf given the parameters
def computeGradientDescentWCF(params):
    #Change: Saturday
    ###y           = open('/Users/anahita/Desktop/train_words.txt')
    
    #initialization of gradient
    gradient1 = np.zeros((10, 321))
    gradient2 = np.zeros((10, 321))
    #for c in range(10):
    #    for f in range(321):
    #        gradient.append(0)
            
            
    n = 400
    #print("WCFWCF")        
    #summation = 0
    for i in range(n):
        #Change: Saturday
        ###word = y.readline()
        word = words[i]
        l = len(word) - 1
        word = word[0:len(word) - 1]
        
        
        #first_term = computeModelCountCF(word, i + 1, c, f)
        feature     = open('/Users/anahita/Desktop/train_img' + str(i + 1) + '.txt')
        observs = feature.read()
        observss = observs.split()
        observations = [float(x) for x in observss]
        #print("MEHRNAZ")
        for j in range(l):
            index = getLabelPos(word, j)
            gradient1[index, 0:321] = gradient1[index, 0:321] + np.asarray(observations[321 * j:321 * j + 321])
            ##for f in range(321):
                ##gradient1[index, f] = gradient1[index, f] + float(observations[321 * j + f])
             
            #Change: Thursday    
            #for c in range(10):
            #print("salam")
            marginal = computeSingleMarginal(l, j, i + 1, params)
                #second_inner_term = computeModelCount(label, i, c, f)
                #for f in range(321):
                    #second_term = second_term + marginal * float(observations[j * 321 + f])
            gradient2 = gradient2 + marginal * np.asarray(observations[j * 321:j * 321 + 321])

        
        #labels = []
        #labels.append("e")
        #labels.append("t")
        #labels.append("a")
        #labels.append("i")
        #labels.append("n") 
        #labels.append("o")
        #labels.append("s")
        #labels.append("h")
        #labels.append("r")
        #labels.append("d")
        #y_all = itertools.product(labels, repeat = l)
    
        #second_term  = 0
        #for y in y_all:
        #label = ''.join(y)
        #feature     = open('/Users/anahita/Desktop/train_img' + str(i + 1) + '.txt')
        #observs = feature.read()
        #observations = observs.split()
        #for j in range(l):
        #    for c in range(10):
        #        marginal = computeSingleMarginal(l, j, i + 1, c, params)
                #second_inner_term = computeModelCount(label, i, c, f)
        #        for f in range(321):
                    #second_term = second_term + marginal * float(observations[j * 321 + f])
        #            gradient2[c, f] = gradient2[c, f] + marginal * float(observations[j * 321 + f])
            
        #summation = summation + (-first_term + second_term)
        
        #print("n" + str(i))
    
    result = np.subtract(gradient2, gradient1)
    result = result / n
    print("wcf")
    #print(result[0, 1])
    #return (summation / n)  
    #Change: Thursday
    #Change: Friday
    ###final_result = []
    ###for c in range(10):
    ###    for f in range(321):
    ###        final_result.append(result[c, f])
            
    ###return final_result
    return result

#p = []
#for i in range(10 * 321):
#    p.append(float(fff[i]))
    
#for j in range(10 * 10):
#    p.append(float(ttt[j]))
    
#print(computeGradientDescentWCF(p))


# In[ ]:




# In[37]:

import numpy
#computes the derivative of the average log likelihood with respect to wcc given the parameters
def computeGradientDescentWTT(params):
    #Change: Saturday
    ###y           = open('/Users/anahita/Desktop/train_words.txt')
    
    n = 400
    
    #initialization
    gradient1 = np.zeros((10, 10))
    gradient2 = np.zeros((10, 10))
    
    #summation = 0
    for i in range(n):
        #Change: Saturday
        ###word = y.readline()
        word = words[i]
        l = len(word) - 1
        word = word[0:len(word) - 1]
        
        
        #first_term = computeModelCountCC(word, i + 1, c, cc)
        for j in range(l - 1):
            index1 = getLabelPos(word, j)
            index2 = getLabelPos(word, j + 1)
            gradient1[index1, index2] = gradient1[index1, index2] + 1
        #labels = []
        #labels.append("e")
        #labels.append("t")
        #labels.append("a")
        #labels.append("i")
        #labels.append("n") 
        #labels.append("o")
        #labels.append("s")
        #labels.append("h")
        #labels.append("r")
        #labels.append("d")
        #y_all = itertools.product(labels, repeat = l)
    
        #second_term  = 0
        #for y in y_all:
        #    label = ''.join(y)
        #for j in range(l - 1):    
        #Change: Thursday
        #for c in range(10):
        #    for cc in range(10):
            marginal = computePairwiseMarginal(l, j, i + 1, params)
                #second_inner_term = computeModelCountCC(label, i, c, cc)
                #second_term = second_term + marginal
            gradient2 = gradient2 + marginal
            
        #summation = summation + (-first_term + second_term)
        
        #print("wcc" + str(i))
    
    #print("wcc")
    #print(summation)
    #return (summation / n)
    result = np.subtract(gradient2, gradient1)
    result = result / n
    print("wcc")
    #print(summation)
    #return (summation / n)     
    #Change: Friday
    ###final_result = []
    ###for c in range(10):
    ###    for cc in range(10):
    ###        final_result.append(result[c, cc])
    ###return final_result
    return result

#p = []
#for i in range(10 * 321):
#    p.append(float(fff[i]))
    
#for j in range(10 * 10):
#    p.append(float(ttt[j]))
    
#print(computeGradientDescentWTT(p))
#print(computeComputeGradientDescentWTT(0, 0, parameters))


# In[38]:

from scipy.optimize import fmin_bfgs
import scipy
#computes derivates of the average log likelihood with respect to both wcf and wcc
def computeDerivatives(params):
    print("params")
    parameters = params.tolist()
    #for s in range(len(parameters)):
    #    print(parameters[s])
    #print(type(params))
    #Change: Friday
    ##content = []
    
    #print("HELLO")
    #for c in range(10):
    #    for f in range(321):
    #        print("Hi" + str(c) + str(f))
    ###content.append(computeGradientDescentWCF(params.tolist()))
    content1 = computeGradientDescentWCF(parameters)
    print("content1")
    ##print(type(content1))
    content1 = content1.flatten()
    ##for c in range(10):
    ##    for cc in range(10):
    ###content.append(computeGradientDescentWTT(params.tolist()))
    content2 = computeGradientDescentWTT(parameters)
    print("content2")
    ##print(type(content2))
    content2 = content2.flatten()
    #print(content2)
    #print(words[0])
    
    print("scipy")
    #print(scipy.array(content))
    #p = 
    #print(p)
    return scipy.array(content1.tolist() + content2.tolist())

#yy = open('/Users/anahita/Desktop/feature-params.txt')
#ff = yy.read()
#fff= ff.split()

#nn = open('/Users/anahita/Desktop/transition-params.txt')
#tt = nn.read()
#ttt = tt.split()

#p = []
#for i in range(10 * 321):
#    p.append(float(fff[i]))
    
#for j in range(10 * 10):
#    p.append(float(ttt[j]))
    
#print(computeDerivatives(p))


# In[41]:

from scipy.optimize import fmin_bfgs
import scipy

#I used the feature and transition parameters given for assignment 2a as initial guess
yy = open('/Users/anahita/Desktop/feature-params.txt')
ff = yy.read()
fff= ff.split()

nn = open('/Users/anahita/Desktop/transition-params.txt')
tt = nn.read()
ttt = tt.split()

par = []
for i in range(10 * 321):
    par.append(float(fff[i]))
    
for j in range(10 * 10):
    par.append(float(ttt[j]))

#yy = open('/Users/anahita/Desktop/param3.txt')
#for s in range(10 * 321 + 10 * 10):
#    po = yy.readline()
#    po = po[0:len(po) - 1]
#    par.append(float(po))
    
y           = open('/Users/anahita/Desktop/train_words.txt')
words = []
for i in range(400):
    word = y.readline()
    words.append(word)
#print(words[0])    
#p = []
#for i in range(10 * 321 + 10 * 10):
#    p.append(0)
#print(params)
#p = scipy.asarray(params)
#print(p)
popt = fmin_bfgs(computeAverageLogLikelihood, x0 = par, fprime = computeDerivatives, disp = True)
for s in range(len(popt)):
    print(popt[s])


# In[ ]:




# In[20]:

import math
#computes the most likely character for the given position j of row = word_number in the test_words.txt given the parameters
def computeMostLikelyProbability(j, word_number, word_length, params):
    ##y           = open('/Users/anahita/Desktop/test_words.txt')
    ##word = ''
    ##for l in range(word_number):
    ##    word = y.readline()
    ##word_length = len(word) - 1
    
    belief = []
    if(j != (word_length - 1)):
        belief = computeBelief(j + 1, word_length, word_number, params)
    else:
        belief = computeBelief(j, word_length, word_number, params)
    
    second_part = 0
    for ym in range(10):
        for yn in range(10):
            second_part = second_part + math.exp(belief[ym * 10 + yn])
            
    joint_prob = []
    for ym in range(10):
        for yn in range(10):
            joint_prob.append(math.exp(belief[ym * 10 + yn]) / second_part)
           
    result = []
    if(j != (word_length - 1)):
        for ym in range(10):
            summation = 0
            for yn in range(10):
                summation = summation + joint_prob[ym * 10 + yn]
            result.append(summation)
    else:
        for yn in range(10):
            summation = 0
            for ym in range(10):
                summation = summation + joint_prob[ym * 10 + yn]
            result.append(summation)
            
    maximum = result[0]
    index = 0
    for i in range(1, len(result)):
        if( result[i] > maximum):
            maximum = result[i]
            index = i
            
    #print("probability= " + str(maximum) + " character= " + str(index))

    return index


# In[21]:

def getLabel(c):
    if(c == 0):
        return 'e'
    if(c == 1):
        return 't'
    if(c == 2):
        return 'a'
    if(c == 3):
        return 'i'
    if(c == 4):
        return 'n'
    if(c == 5):
        return 'o'
    if(c == 6):
        return 's'
    if(c == 7):
        return 'h'
    if(c == 8):
        return 'r'
    if(c == 9):
        return 'd'
    return -1


# In[197]:

#computes the average character-level accuracy
def computeAccuracyCharacterLevel():
    total = 0
    accurate_predictions = 0
    test           = open('/Users/anahita/Desktop/test_words.txt')
    
    yy = open('/Users/anahita/Desktop/params.txt')

    param_list = []
    for p in range(10 * 321 + 10 * 10):
        param = yy.readline()
        param = param[0:len(param) - 1]
        param_list.append(float(param))
    
    for i in range(1, 201):
        word = test.readline()
        word_length = len(word) - 1
        word_number = i
        total = total + word_length
        
        result = []
        for j in range(word_length):
            result.append(computeMostLikelyProbability(j, word_number, len(word) - 1, param_list))
            
        for j in range(word_length):
            if(getLabel(result[j]) == word[j]):
                accurate_predictions = accurate_predictions + 1
                ##print("me")
                
    return (accurate_predictions / total)

print(computeAccuracyCharacterLevel())


# In[22]:

#computes the average word-level accuracy
def computeAccuracyWordLevel():
    total = 0
    accurate_predictions = 0
    test           = open('/Users/anahita/Desktop/test_words.txt')
    
    yy = open('/Users/anahita/Desktop/params.txt')

    param_list = []
    for p in range(10 * 321 + 10 * 10):
        param = yy.readline()
        param = param[0:len(param) - 1]
        param_list.append(float(param))
        
    for i in range(1, 201):
        word = test.readline()
        word_length = len(word) - 1
        word_number = i
        
        result = []
        for j in range(word_length):
            result.append(computeMostLikelyProbability(j, word_number, len(word) - 1, param_list))
            
        same = True
        for j in range(word_length):
            if(getLabel(result[j]) != word[j]):
                ##accurate_predictions = accurate_predictions + 1
                ##print("me")
                same = False
        if(same):
            accurate_predictions = accurate_predictions + 1
         
    total = 200
    print(accurate_predictions)
    return (accurate_predictions / total)

#print(accurate_predictions)
#print(computeAccuracyWordLevel())


# In[ ]:




# In[23]:

b = 1 - 0.9592969472710453
print(b)


# In[ ]:



