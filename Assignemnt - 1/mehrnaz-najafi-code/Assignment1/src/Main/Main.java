package Main;

import java.io.IOException;
import java.util.ArrayList;

import Learning.LearningParams;
import Learning.MyLearningParams;
import Classification.Classfication;
import Classification.MyClassfication;

public class Main {

	public static void main(String[] args) throws IOException{
		
		ArrayList<Float> predictions = new ArrayList<Float>();
		
		//the following block iterates over each train file and test file to first 
		//compute most likely values for hd and then compute mean and standard deviation
		//for the network given in the assigment
		for (int i = 1; i <= 5; i++){
			
			//train data
			String fileNameTrain = "/Users/anahita/Desktop/data-train-" + String.valueOf(i) + ".txt";
			LearningParams lp = new LearningParams(fileNameTrain);
		
			lp.computeA();
			lp.computeG();
			lp.computeBPG();
			lp.computeCHAG();
			lp.computeHDBPCH();
			lp.computeCPHD();
			lp.computeEIAHD();
			lp.computeECGHD();
			lp.computeHRHDA();
			
			//test data
			String fileNameTest = "/Users/anahita/Desktop/data-test-" + String.valueOf(i) + ".txt";
			Classfication m = 
					new Classfication(fileNameTest, lp.a, lp.bpg, lp.chag, lp.cphd, 
							lp.ecghd, lp.eiahd, lp.g, lp.hdchbp, lp.hrhda);
			
			m.computeMostLikelyVariable();
			
			predictions.add(m.getPredictionAccuracy());
			
			System.out.print(m.getPredictionAccuracy() + "\n");
		}
		
		float mean = Classification.Classfication.computeMeanPredictionAccuray(predictions);
		double standardDeviation = Classification.Classfication.
				computeStandardDeviation(predictions, mean);
		
		System.out.print("mean=" + mean + "\n");
		System.out.print("standard deviation=" + standardDeviation);
		
		
		//the following block iterates over each train file and test file to first 
		//compute most likely values for hd and then compute mean and standard deviation
		//for the network that I designed as part of the answer to question 7
		ArrayList<Float> myPredictions = new ArrayList<Float>();
		
		for (int i = 1; i <= 5; i++){
			
			//train data
			String fileNameTrain = "/Users/anahita/Desktop/data-train-" + String.valueOf(i) + ".txt";
			MyLearningParams lp = new MyLearningParams(fileNameTrain);
		
			lp.computeA();
			lp.computeG();
			lp.computeBPAG();
			lp.computeCHAG();
			lp.computeHDBPCH();
			lp.computeCPHD();
			lp.computeEIAHD();
			lp.computeECGHD();
			lp.computeHRHD();
			
			//test data
			String fileNameTest = "/Users/anahita/Desktop/data-test-" + String.valueOf(i) + ".txt";
			MyClassfication m = 
					new MyClassfication(fileNameTest, lp.a, lp.bpag, lp.chag, lp.cphd, 
							lp.ecghd, lp.eiahd, lp.g, lp.hdchbp, lp.hrhd);
			
			m.computeMostLikelyVariable();
			
			myPredictions.add(m.getPredictionAccuracy());
			
			System.out.print(m.getPredictionAccuracy() + "\n");
		}
		
		float myMean = MyClassfication.computeMeanPredictionAccuray(myPredictions);
		double myStandardDeviation = MyClassfication.
				computeStandardDeviation(myPredictions, mean);
		
		System.out.print("my mean=" + myMean + "\n");
		System.out.print("my standard deviation=" + myStandardDeviation);
	}
	
}
