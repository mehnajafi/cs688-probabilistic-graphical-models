package Classification;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

//Classification for the network that I designed as part of the answer to question 7: 
//it finds most likely values of HD for each data case
//it also computes mean and standard deviation as static methods
public class MyClassfication {

	String fileName;
	
	//CPTs for the network that I designed in question 7
	ArrayList<Float> a;
	ArrayList<Float> g;
	ArrayList<Float> chag;
	ArrayList<Float> bpag;
	ArrayList<Float> hdchbp;
	ArrayList<Float> cphd;
	ArrayList<Float> eiahd;
	ArrayList<Float> ecghd;
	ArrayList<Float> hrhd;
	
	float predictionAccuracy;
	
	public MyClassfication(String fileName, ArrayList<Float> a, ArrayList<Float> bpag, 
			ArrayList<Float> chag, ArrayList<Float> cphd, 
			ArrayList<Float> ecghd, ArrayList<Float> eiahd, 
			ArrayList<Float> g, ArrayList<Float> hdchbp, ArrayList<Float> hrhd){
		this.fileName = fileName;
		
		this.a = a;
		this.g = g;
		this.chag = chag;
		this.bpag = bpag;
		this.hdchbp = hdchbp;
		this.cphd = cphd;
		this.eiahd = eiahd;
		this.ecghd = ecghd;
		this.hrhd = hrhd;
	}
	
	//select Age P(A)
	public float selectA(int paramA){
		if(paramA == 1)
			return a.get(0);
		if(paramA == 2)
			return a.get(1);
		if(paramA == 3)
			return a.get(2);
		return 0;
	}
	
	//select Gender P(G)
	public float selectG(int paramG){
		if(paramG == 1)
			return g.get(0);
		return g.get(1);
	}
	
	//select BP | A, G P(BP|A, G)
	public float selectBPAG(String bp, int paramA, int paramG){
		if(bp.equals("1")){
			if(paramG == 1){
				if(paramA == 1)
					return bpag.get(0);
				if(paramA == 2)
					return bpag.get(1);
				if(paramA == 3)
					return bpag.get(2);
			}
			else{
				if(paramA == 1)
					return bpag.get(6);
				if(paramA == 2)
					return bpag.get(7);
				if(paramA == 3)
					return bpag.get(8);
			}
		}
		else{
			if(paramG == 1){
				if(paramA == 1)
					return bpag.get(3);
				if(paramA == 2)
					return bpag.get(4);
				if(paramA == 3)
					return bpag.get(5);
			}
			else{
				if(paramA == 1)
					return bpag.get(9);
				if(paramA == 2)
					return bpag.get(10);
				if(paramA == 3)
					return bpag.get(11);
			}
		}
		return 0;
	}
	
	//select CH | A, G P(CH|A, G)
	public float selectCHAG(String ch, int paramA, int paramG){
		if(ch.equals("1")){
			if(paramA == 1){
				if(paramG == 1)
					return chag.get(0);
				else
					return chag.get(2);
			}
			if(paramA == 2){
				if(paramG == 1)
					return chag.get(4);
				else
					return chag.get(6);
			}
			if(paramA == 3){
				if(paramG == 1)
					return chag.get(8);
				else
					return chag.get(10);
			}
		}
		else{
			if(paramA == 1){
				if(paramG == 1)
					return chag.get(1);
				else
					return chag.get(3);
			}
			if(paramA == 2){
				if(paramG == 1)
					return chag.get(5);
				else
					return chag.get(7);
			}
			if(paramA == 3){
				if(paramG == 1)
					return chag.get(9);
				else
					return chag.get(11);
			}
		}
		return 0;
	}
	
	//HD|BP, CH P(HD|BP,CH)
	public float selectHDBPCH(int hd, String bp, String ch){
		if(hd == 1){
			if(bp.equals("1")){
				if(ch.equals("1"))
					return hdchbp.get(0);
				else
					return hdchbp.get(2);
			}
			else{
				if(ch.equals("1"))
					return hdchbp.get(4);
				else
					return hdchbp.get(6);
			}
		}
		else{
			if(bp.equals("1")){
				if(ch.equals("1"))
					return hdchbp.get(1);
				else
					return hdchbp.get(3);
			}
			else{
				if(ch.equals("1"))
					return hdchbp.get(5);
				else
					return hdchbp.get(7);
			}
		}
	}
	
	//selects CP | HD P(CP|HD)
	public float selectCPHD(int hd, String cp){
		if(hd == 1){
			if(cp.equals("1"))
				return cphd.get(0);
			if(cp.equals("2"))
				return cphd.get(1);
			if(cp.equals("3"))
				return cphd.get(4);
			if(cp.equals("4"))
				return cphd.get(6);
		}
		else{
			if(cp.equals("1"))
				return cphd.get(2);
			if(cp.equals("2"))
				return cphd.get(3);
			if(cp.equals("3"))
				return cphd.get(5);
			if(cp.equals("4"))
				return cphd.get(7);
		}
		
		return 0;
	}
	
	//selects EIA|HD P(EIA|HD)
	public float selectEIAHD(int hd, String eia){
		if(hd == 1){
			if(eia.equals("1"))
				return eiahd.get(0);
			else
				return eiahd.get(1);
		}
		else{
			if(eia.equals("1"))
				return eiahd.get(2);
			else
				return eiahd.get(3);
		}
	}
	
	//selects ECG|HD P(ECG|HD)
	public float selectECGHD(int hd, String ecg){
		if(hd == 1){
			if(ecg.equals("1"))
				return eiahd.get(0);
			else
				return eiahd.get(1);
		}
		else{
			if(ecg.equals("1"))
				return eiahd.get(2);
			else
				return eiahd.get(3);
		}
	}
	
	//selects HR|HD P(HR|HD)
	public float selectHRHD(String hr, int hd){
		if(hr.equals("1")){
			if(hd == 1)
				return hrhd.get(0);
			else
				return hrhd.get(1);
		}
		else{
			if(hd == 1)
				return hrhd.get(2);
			else
				return hrhd.get(3);
		}

	}
	
	//computes the numerator for P(HD|remaining variables)
	public float computeAboveFracHD(int hd, String[] currentLine){
		float firstSum = 0;
		
		for(int paramA =1; paramA <= 3; paramA++)
			for(int paramG = 1; paramG <= 2; paramG++){
				float first = 0;
				//a
				first += selectA(paramA);
				
				//g
				first *= selectG(paramG);
				
				//bp|a, g
				first *= selectBPAG(currentLine[3], paramA, paramG);
				
				//ch | a,g 
				first *= selectCHAG(currentLine[4], paramA, paramG);
				
				//hd | bp, ch
				first *= selectHDBPCH(hd, currentLine[3], currentLine[4]);
				
				//cp |hd
				first *= selectCPHD(hd, currentLine[2]);
				
				//eia | hd
				first *= selectEIAHD(hd, currentLine[7]);
				
				//ecg | hd
				first *= selectECGHD(hd, currentLine[5]);
				
				//hr | hd
				first *= selectHRHD(currentLine[6], hd);
				
				firstSum += first;
			}
		
		return firstSum;
	}
	
	public int computeMostLikelyLine(String[] currentLine){
		
		float firstHD1 = computeAboveFracHD(1, currentLine);
		float firstHD2 = computeAboveFracHD(2, currentLine);
		float secondHD = computeAboveFracHD(1, currentLine) + computeAboveFracHD(2, currentLine);
		
		System.out.print(firstHD1 + "\n");
		//computes P(HD=1|remaining variables)
		float hd1Prob = firstHD1/secondHD;
		//computes P(HD=2|remaining variables)
		float hd2Prob = firstHD2/secondHD;
 		
		if(hd1Prob > hd2Prob)
			return 1;
		return 2;
	}
	
	public void computeMostLikelyVariable() throws IOException{
		FileReader fileReader = new FileReader(fileName);
		
		// Always wrap FileReader in BufferedReader.
		BufferedReader bufferedReader = new BufferedReader(fileReader);
		
		String line;
		int count = 0;
		int correctCase = 0;
		//iterates over each data case to compute most likey value for HD
		while((line = bufferedReader.readLine()) != null) {
			String[] currentLine = line.split(",");
			
			int hd = 1;
			if(currentLine[8].equals("2"))
				hd = 2;
			
			System.out.print("actualHD=" + String.valueOf(hd) + "\n");
			System.out.print("compHD=" + String.valueOf(computeMostLikelyLine(currentLine)) + "\n");
		    if (computeMostLikelyLine(currentLine) == hd){
		    	correctCase ++;
		    }
		    
		    count ++;
		}
		
		System.out.print("correctCase=" + String.valueOf(correctCase) + "\n");
		System.out.print("Total=" + String.valueOf(count) + "\n");
		System.out.print("S" + String.valueOf((float) correctCase/count));
		predictionAccuracy = (float) correctCase/count;
	}
	
	public float getPredictionAccuracy(){
		return predictionAccuracy;
	}
	
	public static float computeMeanPredictionAccuray(ArrayList<Float> pas){
		float sum = 0;
		for(int i = 0; i < pas.size(); i++)
			sum += pas.get(i);
		
		return ((float) (sum/pas.size()));
			
	}
	
	public static double computeStandardDeviation(ArrayList<Float> sts, float mean){
		
		float sumSquares = 0;
		System.out.print("size" + String.valueOf(sts.size()));
		for(int i = 0; i < sts.size(); i++){
			System.out.print("sts" + String.valueOf(sts.get(i)));
			float temp = (sts.get(i) - mean);
			System.out.print("temp" + String.valueOf(temp));
			sumSquares += (temp * temp);
			System.out.print("sumSquares=" + String.valueOf(sumSquares) + "\n");
		}
		
		float deviationSq = (float) sumSquares/sts.size();
		return Math.sqrt(deviationSq);
		
	}
	
}
