package Learning;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

//it learns parameters for the network given in the assignment
public class LearningParams {

	public ArrayList<Float> a;
	public ArrayList<Float> g;
	public ArrayList<Float> chag;
	public ArrayList<Float> bpg;
	public ArrayList<Float> hdchbp;
	public ArrayList<Float> cphd;
	public ArrayList<Float> eiahd;
	public ArrayList<Float> ecghd;
	public ArrayList<Float> hrhda;
	
	String fileName;
	
public LearningParams(String fileName){
	a = new ArrayList<Float>();
	g = new ArrayList<Float>();
	chag = new ArrayList<Float>();
	bpg = new ArrayList<Float>();
	hdchbp = new ArrayList<Float>();
	cphd = new ArrayList<Float>();
	eiahd = new ArrayList<Float>();
	ecghd = new ArrayList<Float>();
	hrhda = new ArrayList<Float>();
		
	this.fileName = fileName;
}
	
//computes CPT for A
public void computeA() throws IOException{
	FileReader fileReader = new FileReader(fileName);

	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float a1 = 0;
	float a2 = 0;
	float a3 = 0;
	float count = 0;
	    
	String line;
	while((line = bufferedReader.readLine()) != null) {
			String[] currentLine = line.split(",");
			count++;
			
			if(currentLine[0].equals("1"))
				a1++;
			if(currentLine[0].equals("2"))
				a2++;
			if(currentLine[0].equals("3"))		
				a3++;
			
	    }
		
	//a[0] A1, a[1] A2, a[2] A3
	a.add(a1/count);
	a.add(a2/count);
	a.add(a3/count);
		
	bufferedReader.close();
}
	
//computes CPT for G
public void computeG() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float g1 = 0;
	float g2 = 0;
	float count = 0;
		    
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
		count++;
				
		if(currentLine[0].equals("1"))
			g1++;
		else
			g2++;
				
	}
			
	//g[0] G1, g[1] G2
	g.add(g1/count);
	g.add(g2/count);
			
	bufferedReader.close();
}

//computes CPT for CH | A, G
public void computeCHAG() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float a1g1 = 0;
	float a1g2 = 0;
	float a2g1 = 0;
	float a2g2 = 0;
	float a3g1 = 0;
	float a3g2 = 0;
		    
	float ch1a1g1 = 0;
	float ch2a1g1 = 0;
	
	float ch1a1g2 = 0;
	float ch2a1g2 = 0;
	
	float ch1a2g1 = 0;
	float ch2a2g1 = 0;
	
	float ch1a2g2 = 0;
	float ch2a2g2 = 0;
	
	float ch1a3g1 = 0;
	float ch2a3g1 = 0;
	
	float ch1a3g2 = 0;
	float ch2a3g2 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
	    //A, G
		if(currentLine[0].equals("1")){
			if(currentLine[1].equals("1")){
				a1g1++;
				if(currentLine[4].equals("1"))
					ch1a1g1++;
				else
					ch2a1g1++;}
			else{
				a1g2++;
				if(currentLine[4].equals("1"))
					ch1a1g2++;
				else
					ch2a1g2++;}
		}
		if(currentLine[0].equals("2")){
			if(currentLine[1].equals("1")){
				a2g1++;
				if(currentLine[4].equals("1"))
					ch1a2g1++;
				else
					ch2a2g1++;}
			else{
				a2g2++;
				if(currentLine[4].equals("1"))
					ch1a2g2++;
				else
					ch2a2g2++;}
		}
		if(currentLine[0].equals("3")){
			if(currentLine[1].equals("1")){
				a3g1++;
				if(currentLine[4].equals("1"))
					ch1a3g1++;
				else
					ch2a3g1++;}
			else{
				a3g2++;
				if(currentLine[4].equals("1"))
					ch1a3g2++;
				else
					ch2a3g2++;}
		}
	}
			
	//
	chag.add(ch1a1g1/a1g1);
	chag.add(ch2a1g1/a1g1);
	
	chag.add(ch1a1g2/a1g2);
	chag.add(ch2a1g2/a1g2);
	
	chag.add(ch1a2g1/a2g1);
	chag.add(ch2a2g1/a2g1);
	
	chag.add(ch1a2g2/a2g2);
	chag.add(ch2a2g2/a2g2);
	
	chag.add(ch1a3g1/a3g1);
	chag.add(ch2a3g1/a3g1);
	
	chag.add(ch1a3g2/a3g2);
	chag.add(ch2a3g2/a3g2);
			
	bufferedReader.close();
}
	
//computes CPT for BP | G
public void computeBPG() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float g1 = 0;
	float g2 = 0;
		    
	float bp1g1 = 0;
	float bp1g2 = 0;
	float bp2g1 = 0;
	float bp2g2 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
	    //BP, G
		if(currentLine[1].equals("1")){
			g1++;
			if(currentLine[3].equals("1"))	
				bp1g1++;
			else
				bp2g1++;
		}
		else{
			g2++;
			if(currentLine[3].equals("1"))	
				bp1g2++;
			else
				bp2g2++;
		}
	}
	
	bpg.add(bp1g1/g1);
	bpg.add(bp1g2/g2);
	bpg.add(bp2g1/g1);
	bpg.add(bp2g2/g2);
	
	bufferedReader.close();
}

//computes CPT for HD | BP, CH
public void computeHDBPCH() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float bp1ch1 = 0;
	float bp1ch2 = 0;
	float bp2ch1 = 0;
	float bp2ch2 = 0;
		    
	float hd1bp1ch1 = 0;
	float hd2bp1ch1 = 0;
	
	float hd1bp1ch2 = 0;
	float hd2bp1ch2 = 0;
	
	float hd1bp2ch1 = 0;
	float hd2bp2ch1 = 0;
	
	float hd1bp2ch2 = 0;
	float hd2bp2ch2 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
	    //BP, CH
		if(currentLine[3].equals("1")){
			if(currentLine[4].equals("1")){
				bp1ch1++;
				if(currentLine[8].equals("1"))
					hd1bp1ch1++;
				else
					hd2bp1ch1++;}
			else{
				bp1ch2++;
				if(currentLine[8].equals("1"))
					hd1bp1ch2++;
				else
					hd2bp1ch2++;}
		}else{
			if(currentLine[4].equals("1")){
				bp2ch1++;
				if(currentLine[8].equals("1"))
					hd1bp2ch1++;
				else
					hd2bp2ch1++;}
			else{
				bp2ch2++;
				if(currentLine[8].equals("1"))
					hd1bp2ch2++;
				else
					hd2bp2ch2++;}
		}
	}
			
	//
	hdchbp.add(hd1bp1ch1/bp1ch1);
	hdchbp.add(hd2bp1ch1/bp1ch1);
	
	hdchbp.add(hd1bp1ch2/bp1ch2);
	hdchbp.add(hd2bp1ch2/bp1ch2);
	
	hdchbp.add(hd1bp2ch1/bp2ch1);
	hdchbp.add(hd2bp2ch1/bp2ch1);
	
	hdchbp.add(hd1bp2ch2/bp2ch2);
	hdchbp.add(hd2bp2ch2/bp2ch2);
			
	//System.out.print(hd1bp1ch2/bp1ch2);
	bufferedReader.close();
}

//computes CPT for CP | HD
public void computeCPHD() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float hd1 = 0;
	float hd2 = 0;
		    
	float cp1hd1 = 0;
	float cp1hd2 = 0;
	float cp2hd1 = 0;
	float cp2hd2 = 0;
	float cp3hd1 = 0;
	float cp3hd2 = 0;
	float cp4hd1 = 0;
	float cp4hd2 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
	    //CP, HD
		if(currentLine[8].equals("1")){
			hd1++;
			if(currentLine[2].equals("1"))	
				cp1hd1++;
			if(currentLine[2].equals("2"))
				cp2hd1++;
			if(currentLine[2].equals("3"))
				cp3hd1++;
			if(currentLine[2].equals("4"))
				cp4hd1++;
		}
		else{
			hd2++;
			if(currentLine[2].equals("1"))	
				cp1hd2++;
			if(currentLine[2].equals("2"))	
				cp2hd2++;
			if(currentLine[2].equals("3"))	
				cp3hd2++;
			if(currentLine[2].equals("4"))	
				cp4hd2++;
		}
	}
	
	cphd.add(cp1hd1/hd1);
	cphd.add(cp2hd1/hd1);
	cphd.add(cp1hd2/hd2);
	cphd.add(cp2hd2/hd2);
	cphd.add(cp3hd1/hd1);
	cphd.add(cp3hd2/hd2);
	cphd.add(cp4hd1/hd1);
	cphd.add(cp4hd2/hd2);
	
	bufferedReader.close();
	
	//System.out.print(cp1hd1/hd1);
}

//computes CPT for EIA | HD
public void computeEIAHD() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float hd1 = 0;
	float hd2 = 0;
		    
	float eia1hd1 = 0;
	float eia1hd2 = 0;
	float eia2hd1 = 0;
	float eia2hd2 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
	    //EIA, HD
		if(currentLine[8].equals("1")){
			hd1++;
			if(currentLine[7].equals("1"))	
				eia1hd1++;
			else
				eia2hd1++;
		}
		else{
			hd2++;
			if(currentLine[7].equals("1"))	
				eia1hd2++;
			else
				eia2hd2++;
		}
	}
	
	eiahd.add(eia1hd1/hd1);
	eiahd.add(eia2hd1/hd1);
	eiahd.add(eia1hd2/hd2);
	eiahd.add(eia2hd2/hd2);
	
	bufferedReader.close();
}

//computes CPT for ECG | HD
public void computeECGHD() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float hd1 = 0;
	float hd2 = 0;
		    
	float ecg1hd1 = 0;
	float ecg1hd2 = 0;
	float ecg2hd1 = 0;
	float ecg2hd2 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
	    //ECG, HD
		if(currentLine[8].equals("1")){
			hd1++;
			if(currentLine[5].equals("1"))	
				ecg1hd1++;
			else
				ecg2hd1++;
		}
		else{
			hd2++;
			if(currentLine[5].equals("1"))	
				ecg1hd2++;
			else
				ecg2hd2++;
		}
	}
	
	ecghd.add(ecg1hd1/hd1);
	ecghd.add(ecg2hd1/hd1);
	ecghd.add(ecg1hd2/hd2);
	ecghd.add(ecg2hd2/hd2);
	
	bufferedReader.close();
}

//computes HR | HD, A
public void computeHRHDA() throws IOException{
	FileReader fileReader = new FileReader(fileName);
	
	// Always wrap FileReader in BufferedReader.
	BufferedReader bufferedReader = new BufferedReader(fileReader);

	float hd1a1 = 0;
	float hd1a2 = 0;
	float hd1a3 = 0;
	float hd2a1 = 0;
	float hd2a2 = 0;
	float hd2a3 = 0;
		    
	float hr1hd1a1 = 0;
	float hr2hd1a1 = 0;
	
	float hr1hd1a2 = 0;
	float hr2hd1a2 = 0;
	
	float hr1hd1a3 = 0;
	float hr2hd1a3 = 0;
	
	float hr1hd2a1 = 0;
	float hr2hd2a1 = 0;
	
	float hr1hd2a2 = 0;
	float hr2hd2a2 = 0;
	
	float hr1hd2a3 = 0;
	float hr2hd2a3 = 0;
	
	String line;
	while((line = bufferedReader.readLine()) != null) {
		String[] currentLine = line.split(",");
				
		if(currentLine[8].equals("1")){
			if(currentLine[0].equals("1")){
				hd1a1++;
				if(currentLine[6].equals("1"))
					hr1hd1a1++;
				else
					hr2hd1a1++;}
			if(currentLine[0].equals("2")){
				hd1a2++;
				if(currentLine[6].equals("1"))
					hr1hd1a2++;
				else
					hr2hd1a2++;}
			if(currentLine[0].equals("3")){
				hd1a3++;
				if(currentLine[6].equals("1"))
					hr1hd1a2++;
				else
					hr2hd1a2++;}
		}else{
			if(currentLine[0].equals("1")){
				hd2a1++;
				if(currentLine[6].equals("1"))
					hr1hd2a1++;
				else
					hr2hd2a1++;}
			if(currentLine[0].equals("2")){
				hd2a2++;
				if(currentLine[6].equals("1"))
					hr1hd2a2++;
				else
					hr2hd2a2++;}
			if(currentLine[0].equals("3")){
				hd2a3++;
				if(currentLine[6].equals("1"))
					hr1hd2a3++;
				else
					hr2hd2a3++;}
		}
	}
			
	//
	hrhda.add(hr1hd1a1/hd1a1);
	hrhda.add(hr2hd1a1/hd1a1);
	
	hrhda.add(hr1hd1a2/hd1a2);
	hrhda.add(hr2hd1a2/hd1a2);
	
	hrhda.add(hr1hd1a3/hd1a3);
	hrhda.add(hr2hd1a3/hd1a3);
	
	hrhda.add(hr1hd2a1/hd2a1);
	hrhda.add(hr2hd2a1/hd2a1);
	
	hrhda.add(hr1hd2a2/hd2a2);
	hrhda.add(hr2hd2a2/hd2a2);
	
	hrhda.add(hr1hd2a3/hd2a3);
	hrhda.add(hr2hd2a3/hd2a3);
			
	bufferedReader.close();
}
}


